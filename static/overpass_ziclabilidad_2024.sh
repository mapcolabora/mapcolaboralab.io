#! /bin/bash

# Script sencillo para bajar datos para mapa uMap

# Vías Ciclables
#wget https://overpass-api.de/api/interpreter\?data\=%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3Barea%283600345740%29%2D%3E%2EsearchArea%3B%28way%5B%22highway%22%3D%22construction%22%5D%5B%22construction%22%3D%22cycleway%22%5D%28area%2EsearchArea%29%3Bway%5B%22construction%3Ahighway%22%3D%22cycleway%22%5D%28area%2EsearchArea%29%3B%29%3Bout%3B%3E%3Bout%20skel%20qt%3B%0A -O ./data_umap/vias_ciclables.json

wget 'http://overpass-api.de/api/interpreter?data=[out%3Ajson][timeout%3A25]%3Barea(3600345740)-%3E.searchArea%3B(way[%22cycleway%22%3D%22opposite%22](area.searchArea)%3Bway[%22bicycle%22%3D%22yes%22]["highway"!="cycleway"](area.searchArea)%3Bway[%22bicycle%22%3D%22designated%22]["highway"!="cycleway"](area.searchArea)%3B)%3Bout%20body%3B%3E%3Bout%20skel%20qt%3B%0A' -O ./data_umap/vias_ciclables.json

# Carriles bici en construcción
wget https://overpass-api.de/api/interpreter?data=%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3Barea%283600345740%29%2D%3E%2EsearchArea%3B%28way%5B%22highway%22%3D%22construction%22%5D%5B%22construction%22%3D%22cycleway%22%5D%28area%2EsearchArea%29%3Bway%5B%22construction%3Ahighway%22%3D%22cycleway%22%5D%28area%2EsearchArea%29%3B%29%3Bout%3B%3E%3Bout%20skel%20qt%3B%0A -O ./data_umap/carriles_bici_const.json

# Carriles bici unidireccionales
wget "http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(way[%22highway%22=%22cycleway%22][%22oneway%22=%22yes%22](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625);way[%22cycleway:right%22=%22lane%22](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625);way[%22cycleway:left%22=%22lane%22](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out%20body;%3E;out%20skel%20qt;" -O ./data_umap/carriles_bici_unidirec.json

# Carriles bici bidireccionales
wget "http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(way[%22highway%22=%22cycleway%22][%22oneway%22!=%22yes%22](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out%20body;%3E;out%20skel%20qt;" -O ./data_umap/carriles_bici_bidirec.json

# Estaciones BiZi
wget 'http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(node[%22amenity%22=%22bicycle_rental%22][%22network%22=%22BiZi%22][!%22mapillary%22](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out%20body;%3E;out%20skel%20qt;' -O ./data_umap/estaciones_bizi.json

# Estaciones BiZi (con imagen)
wget "http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(node[%22amenity%22=%22bicycle_rental%22][%22network%22=%22BiZi%22][%22mapillary%22](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out%20body;%3E;out%20skel%20qt;" -O ./data_umap/estaciones_bizi_imagen.json

# Otras estaciones de alquiler
wget "http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(node["amenity"="bicycle_rental"]["network"!="BiZi"](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out body;>;out skel qt;" -O ./data_umap/otros_alquiler_bici.json

# Tiendas de bicleteas
wget "http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(node["shop"="bicycle"](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out body;>;out skel qt;" -O ./data_umap/tiendas_bicis.json

# Aparcamientos bici
#wget "http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(node["amenity"="bicycle_parking"][!"mapillary"](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out body;>;out skel qt;" -O ./data_umap/aparcamiento_bicis.json

wget http://overpass-api.de/api/interpreter?data=[out%3Ajson][timeout%3A25]%3B(node["amenity"%3D"bicycle_parking"][!"mapillary"](41.44684402008925%2C-1.2332153320312498%2C41.90636538970964%2C-0.5088043212890625)%3B)%3Bout+body%3B>%3Bout+skel+qt%3B -O ./data_umap/aparcamiento_bicis.json

# Aparcamiento bici (con imagen)
wget 'http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(node["amenity"="bicycle_parking"]["mapillary"](41.44684402008925,-1.2332153320312498,41.90636538970964,-0.5088043212890625););out body;>;out skel qt;' -O ./data_umap/aparcamiento_bicis_imagen.json
