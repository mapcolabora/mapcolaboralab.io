[![Netlify Status](https://api.netlify.com/api/v1/badges/7f76a0f3-0521-4fdc-aaab-58a94d657384/deploy-status)](https://app.netlify.com/sites/mapcolabora/deploys)

[![pipeline status](https://gitlab.com/mapcolabora/mapcolabora.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/mapcolabora/mapcolabora.gitlab.io/commits/master)

Este es un prototipo de web para mapeado colaborativo usando [hugo](https://gohugo.io) y alojado en gitlab pages.

Instrucciones para usar en local:

1. Clonar el repositorio: `git clone git@gitlab.com:mapcolabora/mapcolabora.gitlab.io.git`
2. Iniciar submodulos: `git submodule init`
3. Ejecutar script para actualizar el theme: `./update_academic.sh`

Para correr HUGO en nuestro ordenador con una imagen Docker:

```bash
docker run --rm -it \
  -v $PWD:/src      \
  -p 1313:1313      \
  klakegg/hugo:0.53-ext \
  server --disableFastRender 
```

Se pueden añadir estas otras opciones al servidor `--cleanDestinationDir --forceSyncStatic --ignoreCache`.

Mientras esté corriendo este comando la página, si hay cambios, se irá regenerando y se podrán ver al momento en local.


También se ha añadido un archivo docker-compose.yml en el repositorio que se puede ejecutar del siguiente modo:

```bash
# arrancar servicio en local
docker-compose up -d
# parar servicio en local
docker-compose down
```