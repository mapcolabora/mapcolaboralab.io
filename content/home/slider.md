+++
# Slider widget.
widget = "slider"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 1  # Order that this section will appear.

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval = 7000

# Minimum slide height.
# Specify a height to ensure a consistent height for each slide.
height = "500px"

# Slides.
# Duplicate an `[[item]]` block to add more slides.

# Nuevas cartografías colaborativas al alcance de todos -----------------------

[[item]]
  title = "Nuevas cartografías colaborativas al alcance de todos"
  content = """Espacio para la creación de mapas colaborativos al servicio de la ciudadanía y
como herramientas para la toma de decisiones, compartir experiencias y configurar la forma a través de la que observamos la realidad."""
  align = "center"  # Choose `center`, `left`, or `right`.

  # Overlay a color or image (optional).
  #   Deactivate an option by commenting out the line, prefixing it with `#`.
  overlay_color = "#000"  # An HTML color value.
  overlay_img = "headers/Ziclabilidad.png"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.5  # Darken the image. Value in range 0-1.

  # Call to action button (optional).
  #   Activate the button by specifying a URL and button label below.
  #   Deactivate by commenting out parameters, prefixing lines with `#`.
  cta_label = "Saber más"
  cta_url = "/about"
  cta_icon_pack = "fas"
  cta_icon = "info"

# Actividades -----------------------------------------------------------------

[[item]]
  title = "Actividades"
  content = """Realizamos reuniones y actividades abiertas a todo el mundo y para todos los niveles. Síguenos en las redes y únete a nuestra lista de correo para más información."""
  
  align = "center"

  overlay_color = "#555"  # An HTML color value.
  overlay_img = "headers/pano-grupo.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.5  # Darken the image. Value in range 0-1.

  # Call to action button (optional).
  #   Activate the button by specifying a URL and button label below.
  #   Deactivate by commenting out parameters, prefixing lines with `#`.
  cta_label = "Lista de correos"
  cta_url = "https://lists.osgeo.org/mailman/listinfo/mapcolabora"
  cta_icon_pack = "fas"
  cta_icon = "mail-bulk"

# ¿Qué hacemos? ---------------------------------------------------------------

[[item]]
  title = "¿Qué hacemos?"
  content = """Realizamos actividades y proyectos variados sobre mapeado voluntario usando herramientas, servicios y datos libres (como OpenStreetMap, QGIS, Leaflet) y nos centramos en aquellas iniciativas de formación y con fuerte componente social. Visita nuestro [blog](/post) o el [listado de proyectos](/#projects) para conocer más."""
  align = "left"

  overlay_color = "#000"  # An HTML color value.
  overlay_img = "headers/fieldpapers.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.7  # Darken the image. Value in range 0-1.

  # Call to action button (optional).
  #   Activate the button by specifying a URL and button label below.
  #   Deactivate by commenting out parameters, prefixing lines with `#`.
  cta_label = "Conoce nuestros proyectos"
  cta_url = "#projects"
  cta_icon_pack = "fas"
  cta_icon = "info-circle"

+++
