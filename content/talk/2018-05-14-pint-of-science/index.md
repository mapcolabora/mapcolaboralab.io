+++
title = "Sobre sillas, sillitas, bordillos y semáforos sonoros. #zaccesibilidad para la mejora de la movilidad de las personas con diversidad funcional"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2018-05-14T18:00:00
date_end = 2018-05-14T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2018-05-14T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["carlos-camara","miguel-sevilla"]

# Location of event.
location = "Bar Harlem, Zaragoza"

# Name of event and optional event URL.
event = "Pint of Science 2018"
event_url = "http://pintofscience.es/event/de-retrones-y-satelites"

# Abstract. What's your talk about?
abstract = "En un momento en el que existen mapas sobre casi todo, un grupo de personas hemos decidido dedicar nuestro tiempo libre a realizar, entre todos, un mapa de bordillos, semáforos y aceras. Puede parecer algo irrelevante o, en el mejor de los casos, una afición como cualquier otra, pero nada más alejado de la realidad. En esta sesión hablaremos de por qué hacemos ese mapa, para qué sirve, cómo lo hacemos y a quién ayudamos al hacerlo."

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Accesibilidad", "OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "https://docs.google.com/presentation/d/1DrJLiy1Anz5QVmtEzVSutUwgi11ed6GU3Sj0xwLKS1Q/present?token=AC4w5Vi4YPTT9iagAnw9BGmGWbIeXDUpcg%3A1526741667308&ouid=109388546839766604244&includes_info_params=1&noreplica=1&slide=id.g3a8ce69d2b_1_78"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = "Tenemos un problema cuando no existen mapas sobre discapacidad"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++
