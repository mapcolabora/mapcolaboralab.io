+++
title = "El uso de una plataforma cartográfica libre, OpenStreet Map, para el mapeado colaborativo de la ciclabilidad de Zaragoza"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2017-04-28T13:00:00
date_end = 2030-06-01T15:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2017-04-28T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["alejandro-suarez", "hector-ochoa", "miguel-sevilla", "carlos-camara"]

# Location of event.
location = "Zaragoza"

# Name of event and optional event URL.
event = "XIV Congreso Ibérico 'La Bicicleta y la Ciudad'"
event_url = "http://laciudaddelasbicis.org"

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = "Esta comunicación pone de relieve el valor de OpenStreetMap como herramienta para la divulgación y difusión de vías ciclables e infraestructuras afines al desplazamiento en bicicleta tomando como ejemplo la ciudad de Zaragoza."

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["ziclabilidad", "movilidad", "bicis", "OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = "https://docs.google.com/presentation/d/1n3-xEJXI50uaiAG83A18JeAmN3EM9qkzGLEDOcFVYiE/embed?start=false&loop=false&delayms=3000"

# Optional filename of your slides within your talk folder or a URL.
url_slides = ""

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["ziclabilidad"]

# Links (optional).
url_pdf = "https://archive.org/details/comunicacion-OSM-la-ciudad-de-las-bicis-2017/page/n13"
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++

El desarrollo tecnológico y la proliferación de iniciativas colaborativas en Internet ha propiciado la aparición de OpenStreetMap, un proyecto de cartografía temática libre orientado a distribuir datos geográficos de forma abierta. Frente a otros servicios de cartografía en línea, OpenStreetMap se ha convertido en la plataforma de mapas usada en numerosas webs, se está empleando en áreas de asistencia humanitaria, ayuda al desarrollo, en la creación de cartografía alternativa - e. g. para personas con algún tipo de discapacidad - y en ella se está volcando información territorial procedente de servicios locales, regionales o europeos.

Las nuevas tecnologías también se han trasladado a los desplazamientos en bici, haciéndose imprescindible una información geográfica de calidad relativa a este medio de transporte. Cada vez son más habituales las aplicaciones móviles para calcular rutas o realizar el seguimiento de la actividad física. En este sentido es importante el origen, la fiabilidad y actualidad de la información que se maneja.

Esta comunicación pone de relieve el valor de OpenStreetMap como herramienta para la divulgación y difusión de vías ciclables e infraestructuras afines al desplazamiento en bicicleta tomando como ejemplo la ciudad de Zaragoza.

Metodológicamente se procedió en tres fases:

1. Durante la primera fase se recopiló información relevante para los ciclistas como el trazado de vías aptas para ir en bicicleta, los aparcabicis, las fuentes o las estaciones del servicio municipal de alquiler de bicicletas.
2. Para la segunda fase se contó con un sencillo editor implementado dentro de la propia web de OpenStreetMap y en ella se procedió; primero, a incluir y/o contrastar la información de ediciones anteriores y, después, a asignar etiquetas informativas: tipo de elemento, características, superficie, etc.
3. La última fase se realiza con la ayuda del la herramienta Umap que permite la creación de mapas personalizados realzando los elementos que se deseen desde OpenStreetMap y que permite visualizar el resultado del trabajo de la comunidad.

En Zaragoza no existe una plataforma de cartografía colaborativa y digital de la ciclabilidad abierta para todo el mundo y la edición en OSM representa una solución adecuada para este fin.

<iframe src="https://archive.org/embed/comunicacion-OSM-la-ciudad-de-las-bicis-2017" width="100%" height="390" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>
