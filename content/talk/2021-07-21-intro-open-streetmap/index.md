+++
title = "Charla/Taller de introducción a OpenStreetMap"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2021-07-21T18:00:00
date_end = 2021-07-21T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2021-07-25T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla","hector-ochoa","alejandro-suarez","carlos-camara"]

# Location of event.
location = "Taller por videoconferencia"

# Name of event and optional event URL.
event = "Charlas/Talleres del Colegio de Geógrafos de Aragón"
event_url = ""

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["OpenStreetMap", "Cartografía colaborativa","Datos libres"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "20210721-intro-osm-geografos-aragon.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++
