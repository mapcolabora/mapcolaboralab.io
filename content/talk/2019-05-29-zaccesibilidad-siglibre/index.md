+++
title = "Accesibilidad urbana con Openstreetmap: las Fuentes (Zaragoza) y Llefià (Badalona)"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-05-29T10:00:00
date_end = 2019-05-29T12:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2019-06-15T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["ignacio-orte","miguel-sevilla","José Luis infante","carlos-camara","hector-ochoa"]

# Location of event.
location = "Facultad de Filosofía y Letras, Universidad de Girona"

# Name of event and optional event URL.
event = "XIII Jornadas SIG Libre (2019)"
event_url = "https://dugi-doc.udg.edu/handle/10256/17266"

# Abstract. What's your talk about?
abstract = "Análisis de la accesibilidad urbana para personas con movilidad reducida a través del uso de información geográfica voluntaria, cartografía colaborativa y herramientas libres. Los casos de los barrios de Las Fuentes en Zaragoza y Llefià en Badalona​"

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Accesibilidad", "OpenStreetMap", "Zaragoza","Badalona"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "https://dugi-doc.udg.edu/bitstream/handle/10256/17297/Orte-AccesibilidadUrbanaOSMA.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++
