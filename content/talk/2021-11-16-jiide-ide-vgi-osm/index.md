+++
title = "Mesa redonda: Las IDE y la IG voluntaria: situación actual y acciones futuras."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2021-11-16T18:00:00
#date_end = 2018-05-14T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2021-12-01T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla"]

# Location of event.
location = "Videoconferencia"

# Name of event and optional event URL.
event = "XII Jornadas Ibéricas de Infraestructura de Datos Espaciales 2021"
event_url = "https://www.idee.es/jiide"

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Información geográfica voluntaria","Infraestructura de Datos Espaciales","OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "mesa_redonda_carto_oficial_y_osm-x_jiide19.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Links (optional).
url_pdf = ""
url_video = "https://www.youtube.com/watch?v=x8dev3jofZE"
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++

{{< youtube x8dev3jofZE >}}

Resumen de la presentación:

- [00:00](https://youtu.be/x8dev3jofZE?t=0s) 🌐 Información Geográfica Voluntaria

  - OpenStreetMap como paradigma tecnológico.
  - Desarrollo con la web 2.0, dispositivos móviles y open data.
  - OpenStreetMap: Wikipedia de mapas, contribución voluntaria y comunidad.

- [02:20](https://youtu.be/x8dev3jofZE?t=140s) 🌍 Alcance y Datos de OpenStreetMap

  - Variedad de datos: desde usos del suelo hasta infraestructuras.
  - 8 millones de editores globales, 150 diarios en España.
  - Base de datos con 8 millones de nodos, crecimiento diario.

- [04:50](https://youtu.be/x8dev3jofZE?t=290s) 🚀 OpenStreetMap: Comunidad y Proyecto

  - OpenStreetMap como comunidad de entusiastas.
  - Más que un mapa: una base de datos y un ecosistema.
  - Importancia de la comunidad voluntaria y su entusiasmo.

- [06:22](https://youtu.be/x8dev3jofZE?t=382s) 🌐 OpenStreetMap y Administración Pública

  - OpenStreetMap como organización horizontal.
  - Comparación con una versión beta en constante progreso.
  - Importancia de la actualización y el trabajo voluntario.

- [07:18](https://youtu.be/x8dev3jofZE?t=438s) 🗺️ OpenStreetMap como IDE

  - OpenStreetMap como Infraestructura de Datos Espaciales (IDE).
  - Componentes espaciales, temáticos y relaciones.
  - Uso de metadatos y tecnologías para compartir información.

- [09:23](https://youtu.be/x8dev3jofZE?t=563s) 📱 Aplicaciones y Usos de OpenStreetMap

  - Diversidad de aplicaciones y servicios en torno a OpenStreetMap.
  - Datos libres bajo licencia para diversos usos.
  - Ejemplos de aplicaciones y usos en áreas como discapacidad visual.

- [11:25](https://youtu.be/x8dev3jofZE?t=685s) 🌋 OpenStreetMap en Situaciones de Emergencia

  - OpenStreetMap como herramienta crucial en emergencias.
  - Ejemplo de la erupción volcánica en La Palma.
  - Colaboración con programas como Copernicus y uso por grandes empresas.

- [12:22](https://www.youtube.com/watch?v=x8dev3jofZE&t=742s) 🗺️ **OpenStreetMap vs. Cartografía Oficial**
  
  - Comparativa entre mapas de OpenStreetMap y Google Maps.
  - Mejora visual al agregar relieve del IGN al mapa de OpenStreetMap.
  - OpenStreetMap puede complementar la información geográfica de referencia.

- [13:42](https://www.youtube.com/watch?v=x8dev3jofZE&t=822s) 🤝 **Colaboración con Organismos Públicos**

  - Agradecimiento a organismos públicos que contribuyen datos a OpenStreetMap.
  - Importancia de la colaboración para enriquecer la plataforma.
  - Ejemplos de importaciones exitosas, como la del catastro.

- [15:07](https://www.youtube.com/watch?v=x8dev3jofZE&t=907s) 🔄 **Licencias y Reconocimiento**

  - Desafíos relacionados con licencias al importar datos de organismos públicos.
  - Necesidad de reconocimiento explícito para utilizar datos con licencias abiertas.
  - Uso de OpenStreetMap con licencia abierta y reconocimiento de fuentes.

- [16:37](https://www.youtube.com/watch?v=x8dev3jofZE&t=997s) ⚖️ **Ventajas y Desventajas de la Colaboración**

  - Ventajas: Plataforma global de datos, alta actualización, seguimiento de cambios.
  - Desventajas: Posibles conflictos de licencias, riesgo de vandalismo.
  - OpenStreetMap como paso previo para la información de referencia.

- [18:50](https://www.youtube.com/watch?v=x8dev3jofZE&t=1130s) 🌐 **Trabajo en Comunidad y Transparencia**

  - Trabajo en comunidad: intercambio de correos, debate en listas y herramientas.
  - Llamado a unirse, compartir y explorar la transparencia de las discusiones.
  - Destacar la importancia de la comunidad entusiasta de OpenStreetMap.

- [24:31](https://www.youtube.com/watch?v=x8dev3jofZE&t=1471s) 🗺️ La importancia de la colaboración entre OpenStreetMap y organismos públicos.

  - La cartografía de referencia es esencial para la toma de decisiones y la actualización del territorio.
  - OpenStreetMap no reemplaza la cartografía oficial, pero puede complementarla.
  - Ejemplos de situaciones donde la colaboración podría ser beneficiosa para la actualización de la cartografía.

- [29:52](https://www.youtube.com/watch?v=x8dev3jofZE&t=1792s) 🌐 Gemelos digitales del territorio y certificación de representación.

  - Javier explora la creación de gemelos digitales del territorio en áreas urbanas.
  - Destaca la importancia de definir la función y la forma en los gemelos digitales.
  - La certificación de gemelos digitales depende de la aplicación específica y su función.

- [31:42](https://www.youtube.com/watch?v=x8dev3jofZE&t=1902s) 🔄 Normas y estándares en OpenStreetMap.

  - Discusión sobre la interoperabilidad de la información en OpenStreetMap.
  - La comunidad sigue estándares y normas para etiquetar y consultar datos.
  - Posibilidad de consulta de datos en la base de OpenStreetMap para ciertos estándares.

- [34:07](https://www.youtube.com/watch?v=x8dev3jofZE&t=2047s) 🤝 Colaboración activa de las administraciones en OpenStreetMap.

  - Miguel sugiere que las administraciones deberían ser más activas en OpenStreetMap.
  - Propone que las instituciones ayuden a validar y actualizar la información en OpenStreetMap.
  - Destaca los beneficios de tener información oficial y de referencia en la plataforma.

- [36:52](https://www.youtube.com/watch?v=x8dev3jofZE&t=2212s) 🌍 Contribuciones de la comunidad a OpenStreetMap y la importancia de la colaboración.

  - Exploración de cómo la comunidad puede contribuir a importar datos y modificarlos.
  - Importancia de seguir cambios en la cartografía para garantizar su utilidad.
  - Propuesta de establecer un trabajo conjunto para mejorar la calidad de los datos.

- [42:10](https://www.youtube.com/watch?v=x8dev3jofZE&t=2530s) 🤝 Colaboración de la comunidad de OpenStreetMap con entidades públicas.

  - Reflexión sobre cómo la comunidad puede colaborar con entidades públicas productoras de cartografía.
  - Desafíos y beneficios de trabajar juntos en proyectos cartográficos.
  - Mención de la importancia de mejorar la relación entre la comunidad y las instituciones oficiales.

- [44:13](https://www.youtube.com/watch?v=x8dev3jofZE&t=2653s) 🔄 Licenciamiento y colaboración entre OpenStreetMap y entidades oficiales.

  - Problemas relacionados con licencias y cómo afectan la colaboración bidireccional.
  - Propuesta de encontrar soluciones para permitir el uso eficiente de datos de OpenStreetMap por entidades oficiales.
  - La necesidad de trabajar juntos para superar las barreras legales y técnicas.
