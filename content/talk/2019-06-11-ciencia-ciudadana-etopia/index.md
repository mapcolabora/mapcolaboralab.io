+++
title = "Introducción a OSM"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-06-11T18:00:00
#date_end = 2018-05-14T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2018-05-14T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["joan-cano"]

# Location of event.
location = "Etopia. Zaragoza"

# Name of event and optional event URL.
event = "Ciencia ciudadana y ciudad"
event_url = "https://pulsar.unizar.es/charla-taller-vmp-y-openstreetmap/"

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "https://docs.google.com/presentation/d/1x6u-VmJxRm0W0s9JmOc-vUQqkY54IDPnwyzgQxYWFYc/edit?usp=sharing  "

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++

Participación en la semana de la [cienciaciudadana](https://twitter.com/hashtag/cienciaciudadana?src=hashtag_click) en Zaragoza, en la cual se realizaron cinco jornadas para conocer y participar en charlas, talleres y exposiciones que presentan el trabajo de la ciencia ciudadana, abierta y colaborativa.

Nuestra labor fue la de organizar y dirigir un taller para la creación de mapas colaborativos al servicio de la ciudadanía y para dar a conocer su utilidad como herramientas para la toma de decisiones, compartir experiencias y configurar la forma a través de la que observamos la realidad.
