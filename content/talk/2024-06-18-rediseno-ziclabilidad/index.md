+++
title = "Taller de rediseño de #Ziclabilidad"
subtitle = "La plataforma interactiva de la infraestructura ciclista de Zaragoza" 

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2024-06-18T18:30:00
date_end = 2024-06-18T20:30:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2024-06-18T20:30:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["hector-ochoa"]

# Location of event.
location = "Sala La Refinería de La Azucarera (Zaragoza Activa), Zaragoza"

# Name of event and optional event URL.
event = "Actividades del grupo"
event_url = "https://www.zaragoza.es/zac/events/74156"

# Abstract. What's your talk about?
abstract = "Ya en 2017 el Grupo Residente Mapeado Colaborativo utilizaron OpenStreetMap para crear [\"Ziclabilidad\"](/project/ziclabilidad/), la herramienta más detallada sobre infraestructura ciclista de Zaragoza. Una vez los datos estuvieron listos, preparamos un visualizador interactivo de estos. Después de 7 años,esta infraestructura ciclista se ha quedado desfasada y debería rediseñarse, ajustándose a las necesidades de los usuarios. El objetivo de este taller es co-diseñar una herramienta de visualización de infraestructura ciclista y creación de rutas en Zaragoza. Este rediseño seguirá la metodología de pensamiento de diseño (design thinking), que obligará a los asistentes a empatizar con los usuarios de dicha aplicación, y llegar a conocer sus necesidades."

# Summary. An optional shortened abstract.
summary = "El objetivo de este taller es co-diseñar una herramienta de visualización de infraestructura ciclista y creación de rutas en Zaragoza. Este rediseño seguirá la metodología de pensamiento de diseño (design thinking), que obligará a los asistentes a empatizar con los usuarios de dicha aplicación, y llegar a conocer sus necesidades."

# Is this a featured talk? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Movilidad", "Bicicletas", "Co-diseño", "Interfaz"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "20240618-ziclabilidad.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["ziclabilidad"]

# Links (optional).
# url_pdf = ""
# url_video = ""
# url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = "Foto de las diapositivas y la pizarra"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++
