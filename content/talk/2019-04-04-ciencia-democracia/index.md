+++
title = "Debate Ciencia y Democracia"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-04-04T19:00:00
#date_end = 2018-05-14T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2020-02-23T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla"]

# Location of event.
location = "Universidad de Zaragoza"

# Name of event and optional event URL.
event = "Geoforo_xNCT"
event_url = "https://geografia.unizar.es/noticias/geoforociclo-debateabril-2019"

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = ""

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

+++


El 4 de abril, nuestro compañero Miguel Sevilla participó como ponente en el Geoforo por una Nueva Cultura de la Tierra organizado del 4 al 11 de abril de 2019, cuyas sesiones se desarrollaron en el Salón de Actos del edificio de Ciencias Geológicas del Campus universitario de la Pza. San Francisco, Zaragoza, a las 19:00 h.

El objetivo de la primera mesa fue mostrar y discutir experiencias de generación y divulgación de ciencia por parte de personas ajenas a los "círculos académicos convencionales”, como forma, de alguna manera, de ‘democratizar la ciencia’ y hacerla más cercana a los ciudadanos. Fue en esta mesa donde Miguel presentó el proyecto de Mapeado Colaborativo en Zaragoza compartiendo mesa junto al  proyecto RESECOM de seguimiento de plantas y hábitats en Aragón (Begoña García, IPE), y las actividades de difusión en temas de meteorología y astronomía llevadas a cabo por Vicente Aupí en su condición de periodista, escritor y divulgador.
