+++
title = "Mapeado Colaborativo en OSCE Days"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2017-06-12T13:00:00
date_end = 2030-06-01T15:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2017-04-28T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla","alejandro-suarez", "hector-ochoa", "carlos-camara"]

# Location of event.
location = "Zaragoza"

# Name of event and optional event URL.
event = "Open Source Circular Economy Days"
event_url = "https://oscedays.org/"

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mapeado colaborativo", "VGI", "OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = "https://docs.google.com/presentation/d/e/2PACX-1vSeWW2cXoE--yisqqWLkCTFkypjOC5VLch8b7cJTy3me-xWN23yWMgnOuT2PJEqBXxi_wce4E9aTKKD/embed?start=false&loop=false&delayms=3000"

# Optional filename of your slides within your talk folder or a URL.
url_slides = ""

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Links (optional).
url_pdf = "/files/20170612presentacion.pdf"
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++
