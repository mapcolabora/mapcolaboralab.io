+++
title = "#Zaccesibilidad: Mapeando el acceso de las personas con discapacidad en Zaragoza"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-05-22T18:00:00
#date_end = 2018-05-14T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2018-05-14T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["carlos-camara", "miguel-sevilla"]

# Location of event.
location = "Sótano Mágico. Zaragoza."

# Name of event and optional event URL.
event = "Pint of Science 2019, Zaragoza"
event_url = "http://pintofscience.es/event/de-tu-casa-a-la-ma"

# Abstract. What's your talk about?
abstract = "Es el segundo año consecutivo que participamos en la semana del #pintofscience, la cual tuvo lugar en el Sótano Mágico. Durante nuestra intervención sobre hablamos sobre cartografía y el análisis de la accesibilidad para peatones y personas con diversidad funcional en Zaragoza que llevamos realizando el grupo de Mapeado Colaborativo (Geoinquietos Zaragoza) usando la plataforma OpenStreetMap, herramientas libres y una aproximación participativa. Esta charla fue una continuación/complemento de la del año pasado."

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["OpenStreetMap", "accesibilidad"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "https://docs.google.com/presentation/d/e/2PACX-1vQkbpo5jsj5PsJAarbt6xsWejrLza4yEeSkq6dVHhvQU0glnw9_JybvgwSzLxGscCdEQv1QzkV85T0K/pub?start=false&loop=false&delayms=3000"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

[[gallery_item]]
album = "gallery"
image = "D7MeGGiWkAE56TY.jpeg"
caption = ""
[[gallery_item]]
album = "gallery"
image = "D7MeGGjXsAYbo7P.jpeg"
caption = ""
[[gallery_item]]
album = "gallery"
image = "D7MeGGkWsAEt7vL.jpeg"
caption = ""
[[gallery_item]]
album = "gallery"
image = "D7MeGGmXoAENDsW.jpeg"
caption = ""

+++


{{< gallery >}} 
