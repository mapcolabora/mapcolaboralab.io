+++
title = "Charla-taller de introducción a OSM"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-02-22T18:00:00
#date_end = 2018-05-14T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2018-05-14T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["hector-ochoa"]

# Location of event.
location = "Universidad de Zaragoza, Campus Río Ebro"

# Name of event and optional event URL.
event = "PULSAR"
event_url = "https://pulsar.unizar.es/charla-taller-vmp-y-openstreetmap/"

# Abstract. What's your talk about?
abstract = "Charla-taller charla-taller introductoria sobre la base de datos geográfica libre y colaborativa de OpenStreetMap y edición, donde se subieron datos de carriles y velocidad máxima. Los datos introducidos sirvieron para poder crear una cartografía más detallada  de la circulación de VMP (Vehículos de Movilidad Personal) donde tienen cabida los patinetes eléctricos, los cuales cuentan desde 2019 con una nueva normativa."

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "https://isc.unizar.es/index.php/s/dtNaWXHXbYxrT3E"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++

![](photo_2019-02-22_13-17-35.jpg)

Del [blog de Pulsar](https://pulsar.unizar.es/charla-taller-vmp-y-openstreetmap/):

> Hoy hemos tenido una charla-taller introductoria sobre la base de datos geográfica libre y colaborativa de OpenStreetMap. Ahora mismo está en periodo de información pública [la nueva ordenanza de regulación de los vehículos de movilidad personal (VMP)](https://www.zaragoza.es/ciudad/normativa/detalle_Normativa?id=10286).
>
>El texto indica la asimilación en circulación a las bicicletas, excepto en las calzadas, en las que dependería del número de carriles y la velocidad máxima de la vía.
>
>Es por eso que queremos mapear número de carriles y velocidad máxima de todas las calles de la ciudad, para cuando esté la normativa aprobada se pueda sacar un mapa de circulación de estos vehículos.
>
>Os dejamos a continuación las diapositivas y chuleta de la sesión. ¡Nos vemos en la próxima!
>https://isc.unizar.es/index.php/s/dtNaWXHXbYxrT3E
