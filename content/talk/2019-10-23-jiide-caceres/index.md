+++
title = "Cartografía oficial vs. no oficial: fomentando la cooperación"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-10-23T18:00:00
#date_end = 2018-05-14T20:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2018-05-14T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla"]

# Location of event.
location = "Cáceres"

# Name of event and optional event URL.
event = "X Jornadas Ibéricas de Infraestructura de Datos Espaciales 2019"
event_url = "https://www.idee.es/jiide"

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Información geográfica voluntaria","OpenStreetMap","Cooperación"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "mesa_redonda_carto_oficial_y_osm-x_jiide19.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Links (optional).
url_pdf = ""
url_video = "https://youtu.be/qOwmL3td7Dc?t=1280"
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++

Miguel Sevilla participó en las  #JIIDE en #Cáceres, un evento organizado por la Infraestructura de Datos Espaciales de España (IDEE)  y el los miembros del SIG municipal del Ayuntamiento de Cáceres. Nuestro compañero habló sobre "Cartografía oficial vs no oficial: Fomentando la cooperación"

{{< youtube qOwmL3td7Dc >}}

### Ideas de la presentación

- La información geográfica voluntaria, como la proporcionada por OpenStreetMap, se ha vuelto una alternativa a los servicios de mapas en línea centralizados.

- OpenStreetMap es una base de datos espacial abierta y colaborativa con 5.7 millones de usuarios registrados a nivel mundial.

- Se destaca por ser una alternativa gratuita y precisa para la generación de mapas.

- La información geográfica alternativa es crucial en áreas donde los organismos públicos no llegan, como en países en vías de desarrollo.

- Se menciona el ejemplo de la colaboración con Médicos Sin Fronteras para mapear en Mozambique.
- La comunidad de OpenStreetMap es esencial, con grupos de trabajo, herramientas de validación, editores móviles y aplicaciones colaborativas que promueven la participación.

- Se resalta la importancia de la colaboración pública-privada y se invita a las instituciones a unirse a la comunidad.

- El modelo de datos de OpenStreetMap se basa en nodos, líneas y polígonos, con etiquetas que proporcionan información espacial detallada.

- Las relaciones permiten organizar conjuntos de datos más complejos.

- Los metadatos y las decisiones colectivas se documentan en la wiki de OpenStreetMap, sirviendo como referencia para la comunidad.

- La wiki contiene información sobre etiquetado y decisiones tomadas en reuniones comunitarias.

- OpenStreetMap es editado por cualquier persona, pero hay herramientas de edición fácil y una fundación sin ánimo de lucro respalda la plataforma.

- La edición se asemeja al modelo de Wikipedia, con un proceso de verificación de calidad.

- Se muestra el editor en la página web, destacando su simplicidad y funcionalidad.

- La calidad de los datos en OpenStreetMap varía, mostrando ejemplos comparativos con mapas topográficos nacionales y Google Maps.

- Se destaca la importancia de la colaboración con instituciones como el IGN y el Catastro.

- La licencia de OpenStreetMap prohíbe la importación directa de datos con licencia Creative Commons, requiriendo autorización específica de los organismos oficiales.

- Se menciona un proceso estricto de importación, ejemplificado con la importación de datos de edificaciones del Catastro.

- La comunidad de OpenStreetMap realiza importaciones de datos de manera voluntaria y en tiempo libre, siguiendo protocolos y validaciones para garantizar la calidad de la información espacial.

### Secciones y conceptos clave (por marcas de tiempo)

- [21:06](https://youtu.be/qOwmL3td7Dc?t=1266s) 📚 **Información de Referencia en OpenStreetMap:**
  - Diferencia entre "cartografía oficial" y "cartografía colaborativa" en OpenStreetMap.
  - La revolución de la información geoespacial voluntaria y su popularización.
  - Iniciativas colaborativas frente a proyectos centralizados, destacando la variedad de datos.

- [23:28](https://youtu.be/qOwmL3td7Dc?t=1408s) 🌍 **Impacto de la Información Geográfica Voluntaria:**
  - La capacidad de cualquier persona para mapear y contribuir a OpenStreetMap.
  - Ejemplos de edición colaborativa, desde guaridas de castores hasta cambios en bancos y papeleras.
  - La información geográfica voluntaria como una nueva forma de generar datos de manera bottom-up.

- [36:05](https://youtu.be/qOwmL3td7Dc?t=2165s) 🗺️ Licencia y Cooperación con Organismos Oficiales
  - OpenStreetMap utiliza una licencia tipo Creative Commons BY 4.0 para su base de datos espacial.
  - Para importar datos, se requiere autorización específica de los organismos oficiales, como en el caso del catastro.
  - El proceso de importación sigue protocolos estrictos, como se evidencia en la importación de edificaciones con autorización del catastro.

- [37:55](https://youtu.be/qOwmL3td7Dc?t=2275s) 🏢 Importación de Edificaciones del Catastro
  - El ejemplo muestra el proceso de importación de edificaciones desde el catastro a OpenStreetMap.
  - La validación es esencial para garantizar la precisión y evitar conflictos con edificaciones ya editadas por la comunidad.  
  - La importación se realiza de manera voluntaria, municipio por municipio, con la participación activa de la comunidad.
