+++
title = "Cómo hacer consultas personalizadas para descargar datos de OSM"
subtitle = "Overpass y otras herramientas" 

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2024-05-28T18:30:00
date_end = 2024-05-28T20:30:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2024-05-28T20:30:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["hector-ochoa"]

# Location of event.
location = "Sala La Colaboradora de La Azucarera (Zaragoza Activa), Zaragoza"

# Name of event and optional event URL.
event = "Actividades del grupo"
#event_url = "https://www.zaragoza.es/zac/events/XXXXX"

# Abstract. What's your talk about?
abstract = "Este martes 28 de mayo hemos realizado una presentación sobre cómo descargar datos de OpenStreetMap (OSM). Durante la presentación se ha explicado brevemente la estructura de datos de OSM, y cómo buscar en [la Wiki](https://wiki.openstreetmap.org/wiki/ES:Objetos_del_mapa) el etiquetado de diferentes elementos. Además, se han mostrado diversas maneras de descargar datos, desde extractos con los datos completos en una región (rectangular o región política), a consultas personalizadas mediante la interfaz [Overpass Turbo](https://overpass-turbo.eu/). También, se ha introducido un [trabajo reciente de Staniek et al.](https://doi.org/10.1162/tacl_a_00654), donde se generan consultas en el lenguaje Overpass QL a través de entradas en texto natural, que son tratadas por un Modelo de Lenguaje Grande (LLM por sus siglas en inglés)."

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Información Geográfica Voluntaria", "VGI", "Mapeado colaborativo", "OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "20240528-overpass.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Links (optional).
# url_pdf = ""
# url_video = ""
# url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = "Diapositiva de la presentación"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++
