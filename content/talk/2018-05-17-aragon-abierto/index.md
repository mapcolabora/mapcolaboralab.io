+++
title = "La imagen del mundo y las proyecciones cartográficas"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2018-05-17T19:00:00
date_end = 2018-05-17T19:05:00
all_day = true

# Schedule page publish date (NOT talk date).
publishDate = 2018-05-14T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla", "carlos-camara"]

# Location of event.
location = "Zaragoza"

# Name of event and optional event URL.
event = "Aragón en Abierto"
event_url = "http://alacarta.aragontelevision.es/programas/aragon-en-abierto/"

# Abstract. What's your talk about?
abstract = "Intervención en el programa Aragón en Abierto (Aragón TV) explicando el problema de la traslación a un plano de la superficie esférica de la tierra."

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Mapas", "Proyecciones"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = ""

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Links (optional).
url_pdf = ""
url_video = "http://alacarta.aragontelevision.es/programas/aragon-en-abierto/jueves-17-de-mayo-17052018-1800"
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++

{{< gallery >}} 
<!-- ![](gallery/proyecciones-aragontv.jpg)
![](gallery/photo_2018-05-21_18-26-53.jpg)-->
