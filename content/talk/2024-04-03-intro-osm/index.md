+++
title = "Introducción al mapeado colaborativo con OpenStreetMap"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2024-04-02T18:30:00
date_end = 2024-04-02T20:30:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2024-04-03T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla"]

# Location of event.
location = "Sala multimedia de La Azucarera (Zaragoza Activa), Zaragoza"

# Name of event and optional event URL.
event = "Actividades del grupo"
event_url = "https://www.zaragoza.es/zac/events/73036"

# Abstract. What's your talk about?
abstract = "Presentación de introducción a OpenStreetMap e inicio en la edición con el editor iD"

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Información Geográfica Voluntaria", "VGI", "Mapeado colaborativo", "OpenStreetMap"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "20240402-intro-OSM_.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Links (optional).
# url_pdf = ""
# url_video = ""
# url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++