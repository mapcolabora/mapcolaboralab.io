+++
title = "Análisis de la accesibilidad urbana para personas con movilidad reducida a través del uso de IG voluntaria, cartografía colaborativa y herramientas libres"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-05-29T19:00:00
date_end = 2019-05-30T19:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2020-02-23T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla", "Ignacio Orte", "Jose Luis Infante", "carlos-camara", "hector-ochoa"]

# Location of event.
location = "Universitat de Girona"

# Name of event and optional event URL.
event = ""
event_url = "https://geografia.unizar.es/noticias/geoforociclo-debateabril-2019"

# Abstract. What's your talk about?
abstract = ""

# Summary. An optional shortened abstract.
summary = ""

# Is this a featured talk? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["OpenStreetMap", "accesibilidad"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "https://docs.google.com/presentation/d/e/2PACX-1vS_w4yhm_96XwLjpLOj8Tfb9pqgO26E0eZYcUD40GNp34h2604Nv5toSEzJ0mFzbeh5kvophPQYlHtr/pub?start=false&loop=false&delayms=3000"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

+++

Las Jornadas de SIG libre son una iniciativa del SIGTE, un congreso dedicado al software libre en el campo de las Tecnologías de la Información Geográfica.
Se han convertido en un punto de encuentro entre las personas que comparten un mismo interés: el uso, el desarrollo y la promoción de los Sistemas de Información Geográfica libres y los datos abiertos en el ámbito de la empresa, la universidad y la administración pública.

Este evento de referencia en el sector es a la vez un espacio de aprendizaje donde compartir e intercambiar experiencias y conocimientos en torno a las soluciones libres en el ámbito de las tecnologías geoespaciales así como mostrar las últimas novedades.


{{< gallery >}}
