+++
# Display name
name = "Héctor Ochoa"

# Username (this should match the folder name)
authors = ["hector-ochoa"]

# Is this the primary user of the site?
superuser = false

# Role/position
role = "Ingeniero de datos geográficos. Investigador multidisciplinar"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "Universidad de Camerino (Università degli Studi di Camerino)", url = "https://unicam.it/" } ]

# Short bio (displayed in user profile at end of posts)
bio = """Máster Erasmus Mundus en Cartografía. Ingeniero Informático por la Universidad de Zaragoza. Apoyando el conocimiento y la cultura libre, apasionado de los mapas, la movilidad urbana y activo contribuidor de OpenStreetMap en la ciudad de Zaragoza."""

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "cie.hochoa@gmail.com"

# List (academic) interests or hobbies
interests = [
  "Estándares abiertos",
  "Conocimiento libre",
  "Mapas",
  "Movilidad"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Equipo de trabajo"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "Doctorado en Informática y Matemáticas (cursando)"
  institution = "Universidad de Camerino"
  year = 2021
[[education.courses]]
  course = "Máster en Cartografía"
  institution = "Título conjunto entre Universidades Técnicas de Múnich, Viena y Dresde; Universidad de Twente"
  year = 2022
[[education.courses]]
  course = "Grado en Ingeniería Informática"
  institution = "Universidad de Zaragoza, intercambio en Universidad Técnica del Mar Negro"
  year = 2020

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "https://twitter.com/hectrenes"

[[social]]
  icon = "linkedin"
  icon_pack = "fab"
  link = "https://www.linkedin.com/in/hector-ochoa-ortiz/"

[[social]]
  icon = "github"
  icon_pack = "fab"
  link = "https://github.com/Robot8A"

[[social]]
  icon = "map-marked-alt"
  icon_pack = "fas"
  link = "https://openstreetmap.org/user/Robot8A"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

Máster Erasmus Mundus en Cartografía. Ingeniero Informático por la Universidad de Zaragoza. Apoyando el conocimiento y la cultura libre, apasionado de los mapas, la movilidad urbana y activo contribuidor de OpenStreetMap en la ciudad de Zaragoza.
