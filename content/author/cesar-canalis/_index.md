+++
# Display name
name = "César Canalís"

# Username (this should match the folder name)
authors = ["cesar-canalis"]

# Is this the primary user of the site?
superuser = false

# Role/position
role = "Arquitecto"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
# organizations = [ {" "} ]

# Short bio (displayed in user profile at end of posts)
bio = """Arquitecto por la Universidad San Jorge, interesado en discapacidad."""

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = ""

# List (academic) interests or hobbies
interests = [
  "Discapacidad",
  "Arquitectura"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Antiguos colaboradores"]

# List qualifications (such as academic degrees)
# [[education.courses]]
#   course = "Doctorado en Sociedad de la Información y el Conocimiento"
#   institution = "Universitat Oberta de Catalunya"
#   year = 2018

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "linkedin"
  icon_pack = "fab"
  link = "https://www.linkedin.com/in/cesarcanalis"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

Arquitecto por la [Universidad San Jorge](https://usj.es), interesado en discapacidad.
