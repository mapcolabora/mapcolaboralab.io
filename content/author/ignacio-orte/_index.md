+++
# Display name
name = "Ignacio Orte"

# Username (this should match the folder name)
authors = ["ignacio-orte"]

# Is this the primary user of the site?
superuser = false

# Role/position
role = "Geógrafo, Máster en Ordenación del Territorio y Medio Ambiente"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "Universidad de Zaragoza", url = "https://unizar.es/" } ]

# Short bio (displayed in user profile at end of posts)
bio = """Geógrafo y Máster en Ordenación del Territorio y Medio Ambiente por la Universidad de Zaragoza."""

# Enter email to display Gravatar (if Gravatar enabled in Config)
# email = ""

# List (academic) interests or hobbies
# interests = ["Sistemas de información geográfica","Energías Renovables","OpenStreetMap","Desarrollo rural"]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Equipo de trabajo"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "Máster en Ordenación del Territorio y Medio Ambiente"
  institution = "Universidad de Zaragoza"
  year = 2020
[[education.courses]]
  course = "Grado en Geografía"
  institution = "Universidad de Zaragoza"
  year = 2019

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "https://twitter.com/elpezbartolo"

[[social]]
  icon = "linkedin"
  icon_pack = "fab"
  link = "https://www.linkedin.com/in/ignacio-orte-sierra"

# [[social]]
#  icon = "github"
#  icon_pack = "fab"
#  link = "https://github.com/Robot8A"

[[social]]
  icon = "map-marked-alt"
  icon_pack = "fas"
  link = "https://www.openstreetmap.org/user/elpezBartolo"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

<!-- RELLENAR -->
