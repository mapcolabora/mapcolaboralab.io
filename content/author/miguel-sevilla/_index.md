+++
# Display name
name = "Miguel Sevilla-Callejo"

# Username (this should match the folder name)
authors = ["miguel-sevilla"]

# Is this the primary user of the site?
superuser = false

# Role/position
role = "Dr. en Geografía"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "Asociación QGIS España", url = "http://qgis.es" },
  { name = "Instituto Pirenaico de Ecología-CSIC", url = "http://www.ipe.csic.es/" }]

# Short bio (displayed in user profile at end of posts)
bio = """Doctor en Geografía. Investigador. Profesor. Consultor *freelance*. Interesado en las dinámicas territoriales, las tecnologías de la información geográfica y el software y herramientas libres."""

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = ""

# List (academic) interests or hobbies
interests = [
  "Dinámicas territoriales",
  "Tecnologías de la Información Geográfica",
  "Software y datos libres"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Equipo de trabajo", "Coordinación"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "Doctorado en Geografía"
  institution = "Universidad Autónoma de Madrid"
  year = 2010
[[education.courses]]
  course = "Licenciatura en Geografía"
  institution = "Universidad Autónoma de Madrid"
  year = 2000

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "http://twitter.com/msevilla00"

[[social]]
  icon = "mastodon"
  icon_pack = "fab"
  link = "https://mastodon.social/@msevilla00"

[[social]]
  icon = "gitlab"
  icon_pack = "fab"
  link = "https://gitlab.com/msevilla00"

[[social]]
  icon = "map-marked-alt"
  icon_pack = "fas"
  link = "https://openstreetmap.org/user/msevilla00"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

Soy doctor en Geografía, he trabajado en temas sobre desarrollo, mundo rural y la aplicación de tecnologías de la información geográfica en diferentes ámbitos, estuve de profesor asociado en el [departamento de Geografía y Ordenación del Territorio de la Universidad de Zaragoza](https://geografia.unizar.es), soy asistente de investigación en el [Instituto Pirenaico de Ecología del CSIC](https://www.ipe.csic.es), y entre mis aficiones está la participación en las comunidades de software y datos espaciales libres, destacando mi presencia en la junta directiva de la [asociación QGIS España](https://www.qgis.es/asociacion/), la implicación en la [comunidad española de colaboradores de OpenStreetMap](https://wiki.openstreetmap.org/wiki/ES:Espa%C3%B1a) y la coordinación del grupo [Mapeado Colaborativo / Geoinquietos Zaragoza](https://mapcolabora.org).