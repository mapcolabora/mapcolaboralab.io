+++
# Display name
name = "Carlos Cámara-Menoyo"

# Username (this should match the folder name)
authors = ["carlos-camara"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Arquitecto. Doctor. Profesor. Estudiante."

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "University of Warwick", url = "http://warwick.ac.uk/cim" }, { name = "Universitat Oberta de Catalunya", url = "http://uoc.edu"} ]

# Short bio (displayed in user profile at end of posts)
bio = """Mi formación mutidisciplinar y **mis múltiples intereses de investigación se vertebran alrededor de las comodificaciones entre ciudad, tecnología y sociedad** dentro del marco del informacionalismo y la cultura libre, aspectos que abordo tanto desde mi vertiente profesional académica como desde mi vertiente activista"""

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "carlos@carloscamara.es"

# List (academic) interests or hobbies
interests = [
  "Ciudades",
  "Tecnopolítica",
  "Sociedad",
  "Commons",
  "Mapas"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Equipo de trabajo"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "Doctorado en Sociedad de la Información y el Conocimiento"
  institution = "Universitat Oberta de Catalunya"
  year = 2018

[[education.courses]]
  course = "Arquitectura"
  institution = "Universitat Politècnica de Catalunya"
  year = 2004

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "globe"
  icon_pack = "fas"
  link = "https://carloscamara.es/"

[[social]]
  icon = "mastodon"
  icon_pack = "fab"
  link = "https://scholar.social/@ccamara"

[[social]]
  icon = "orcid"
  icon_pack = "ai"
  link = "http://orcid.org/0000-0002-9378-0549"

[[social]]
  icon = "github"
  icon_pack = "fab"
  link = "https://github.com/ccamara"

[[social]]
  icon = "linkedin"
  icon_pack = "fab"
  link = "https://www.linkedin.com/in/carlescamara"

[[social]]
  icon = "map-marked-alt"
  icon_pack = "fas"
  link = "https://openstreetmap.org/user/ccamara"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

Arquitecto por la ETSA Vallès de la Universitat Politècnica de Catalunya y Doctor en Sociedad de la Información y el Conocimiento por la Universitat Oberta de Catalunya. 

Entre mis múltiples intereses de investigación se encuentran las comodificaciones entre ciudad, tecnología y sociedad dentro del marco del informacionalismo y la cultura libre. [+info sobre mi](https://carloscamara.es/about).
