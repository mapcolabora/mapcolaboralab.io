+++
# Project title.
title = "Zaccesible"

authors = ["carlos-camara", "miguel-sevilla", "cesar-canalis", "hector-ochoa", "joan-cano", "alejandro-suarez"]

# Date this page was created.
date = 2016-04-27T00:00:00

# Project summary to display on homepage.
summary = "Un mapa de accesibilidad abierto y colaborativo de Zaragoza."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Movilidad", "Discapacidad", "OpenStreetMap", "en curso"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_slides = ""
url_video = ""
url_code = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{icon_pack = "fas", icon="globe", name="Web del proyecto", url = "https://zaccesible.usj.es"}]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"



[[gallery_item]]
album = "gallery"
image = "Screenshot_20180217_233133.png"
caption = "En el taller de la USJ, con mis compañeros Santiago Elía y David Chinarro y el alumno Enrico Campagna."
+++


**Zaragoza Accesible** es un proyecto de mapeado colaborativo de aspectos relacionados con la discapacidad y la movilidad en Zaragoza desarrollado conjuntamente con el grupo de investigación *Arquitecturas OpenSource* de la Escuela de Arquitectura y Tecnología de la [Universidad San Jorge](http://usj.es).
Gracias a las observaciones de campo sobre realizadas por voluntarias y a OpenStreetMap, esperamos realizar un mapa de toda Zaragoza que facilite la movilidad de las personas con diversidad funcional.

{{< youtube id="iVk5Dyx4Cgk" autoplay="false" >}}

Tenéis más información del proyecto en la [web específica del grupo de investigación](http://zaccesible.usj.es) y en la etiqueta [#Zaccesible](/tags/zaccesible) de nuestro blog.

{{< gallery album="gallery" >}}

Han colaborado también en el proyecto:

* [Discapacitados Sin Fronteras Aragón](http://discapacitadossinfronteras.com/)
* Asociación de Vecinos y Vecinas Manuel Viola
* Centro Cívico Las Esquinas del Psiquiátrico
* [Ganchillo Social](https://elganchillosocial.wordpress.com/), en colaboración con [Ocio Inclusivo](https://ocioinclusivoarrabal.wordpress.com/) y la Asociación de Vecinos del Arrabal.
* Decenas de voluntarios que se han unido a nosotros en las distintas mapping parties que hemos organizado.
