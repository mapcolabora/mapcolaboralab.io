+++
# Project title.
title = "CitMapp"
subtitle = "Aplicación de mapeado para ciencia ciudadana"

authors = ["miguel-sevilla"]

# Date this page was created.
date = 2020-02-10T00:00:00

# Project summary to display on homepage.
summary = "Aplicación de mapeado para ciencia ciudadana"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["ciencia ciudadana","app", "mapeado"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_slides = ""
url_video = ""
url_code = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{icon_pack = "fab", icon="google-play", name="Descargar en Google Play", url = "https://play.google.com/store/apps/details?id=com.ibercivis.mapp2"}]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"

+++

Desarrollada por [IberCivis](https://ibercivis.es/), fundación que da amparo a proyectos de Ciencia Ciudadana, desde el grupo se ha promovido una aplicación móvil de licenciamiento libre que permitirá posicionar elementos y generará datos geográficos colaborativos.

Esta aplicación está no solo orientada para el trabajo del grupo Mapeado Colaborativo si no estará abierto a otras iniciativas que necesiten el geoposicionamiento y la creación de listados de elementos que puedan ser registrados de forma participativa.

La aplicación, es personalizable, permite generar proyectos específicos (cualquiera puede crear su propio proyecto/formulario) y genera salidas para incorporar en bases de datos con datos espaciales.

Puedes ver más información sobre esta aplicación en la entrada que tienen en [Ibercivis en su página web](https://ibercivis.es/citmapp-charla-miguel-sevilla/) y también en [la charla que dió nuestro compañero Miguel Sevilla-Callejo en las jornadas de SIGLibre de 2021](https://diobma.udg.edu//handle/10256.1/6221).

{{< gallery >}}
