+++
# Project title.
title = "Mapatón Humanitario"

authors = ["miguel-sevilla", "hector-ochoa", "alejandro-suarez", "joan-cano", "carlos-camara"]

# Date this page was created.
date = 2016-04-27T00:00:00

# Project summary to display on homepage.
summary = "Mapear para una buena causa: Añadiendo detalles al mapa para MSF"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["en curso", "OpenStreetMap", "MSF", "Médicos Sin Fronteras"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_slides = ""
url_video = ""
url_code = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{icon_pack = "fas", icon="globe", name="Web del proyecto", url = "https:..."}]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "88 En el aula 8 Varios de los participantes, ayer, durante el mapatón. - NACHO BALLARÍN"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"

+++

Explicación aquí

## Edición 2019

**Fecha:** Jueves, 11de abril, de 16:30 a 20:00 h

**Lugar:** Aula de informática V del Edificio Central de Filosofía y Letras

La novedad del "Día del Mapatón Humanitario 2019" ha sido el hecho de hacerlo simultáneamente en 15 sedes en 14 ciudades de España. En palabras de [Médicos Sin Fronteras](https://msf-spain.prezly.com/15-mapatones-simultaneos-en-14-ciudades-de-espana-cartografian-zonas-remotas-y-areas-afectadas-por-catastrofes#):

> "Más de 500 activistas convocados por Médicos Sin Fronteras (MSF), el Departamento de Geografía y Ordenación del Territorio de la Universidad de Zaragoza y Missing Maps han cartografiado zonas remotas en las que intervienen los equipos de MSF en Malí, Nigeria, República Democrática del Congo (RDC) y áreas damnificadas por el ciclón Idai en Mozambique. Los 15 mapatones se han celebrado de forma simultánea con motivo del ‘Día del Mapatón Humanitario 2019’ que la organización médico-humanitaria ha llevado a cabo en A Coruña, Barcelona, Córdoba, Gijón, Girona, Madrid, Mieres, Murcia, Pamplona, Santander, Santiago de Compostela, Sevilla, Valencia y Zaragoza, donde voluntarios han cartografiado de forma colaborativa zonas invisibles del planeta.
>
>Los voluntarios han incrementado el detalle de zonas de las que apenas había información en los mapas digitales convencionales y sobre las que los equipos de ayuda humanitaria necesitan información precisa para sus intervenciones. En los mapatones se ha cartografiado el sur de Douentza en la región de Mopti (Malí); de Nigeria se han mapeado áreas de Gwoza y Pulka, en el estado de Borno, que acogen a decenas de miles de desplazados. En el caso de la RDC, los participantes en los mapatones han trabajado sobre zonas de las regiones de Ituri y Maniema caracterizadas por el surgimiento de brotes epidémicos y de violencia y en los que los desplazamientos de población son recurrentes. Otra de las tareas que han realizado los participantes ha sido agregar detalle a los mapas de zonas de Beira y Dondo afectadas por el ciclón Idai que ha provocado más de 130.000 desplazados y casi 600 fallecidos en Mozambique".

{{< tweet 1116356265775702016 >}}
{{< tweet 1116361987116736512 >}}

{{< gallery album="2019" >}}

## Edición 2018

{{< gallery album="2018" >}}

## Edición 2017

{{< gallery album="2017" >}}
