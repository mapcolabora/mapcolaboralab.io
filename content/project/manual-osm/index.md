+++
# Project title.
title = "Manual de OSM"

# Username (this should match the folder name)
authors = ["carlos-camara", "miguel-sevilla", "hector-ochoa", "alejandro-suarez"]

# Date this page was created.
date = 2016-11-14T00:00:00

# Project summary to display on homepage.
summary = "Coordinación y edición de un manual de introducción a OpenStreetMap realizado de forma colaborativa y abierta con el objetivo de romper las barreras de acceso a OSM y ponerlo al alcance de todo el mundo"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["OpenStreetMap", "paralizado"]

# Optional external URL for project (replaces project detail page).
external_link = ""


# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_slides = ""
url_video = ""
url_code = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{icon_pack = "fas", icon="book", name="Ver manual", url = "https://mapcolabora.gitbooks.io/manual-osm/content/"}]


# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++

Coordinación y edición de un manual de introducción a OpenStreetMap realizado de forma colaborativa y abierta con el objetivo de romper las barreras de acceso a OSM y ponerlo al alcance de todo el mundo.

## ¿Por qué un libro colaborativo de OSM?

Es cierto que hay mucha información sobre OSM y podría parecer redudante: entre otros, están la [wiki oficial](http://wiki.openstreetmap.org/) (Tanto en inglés como en español), o recursos como [learnosm](http://learnosm.org/). Sin embargo, aunque son recursos muy valiosos a menudo o no están totalmente disponibles en español o bien para alguien que se quiere iniciar en OSM puede que le resulte abrumadora toda la información disponible (y no siempre estructurada).

Este libro, por tanto, tiene el doble objetivo de recopilar y estructurar de forma clara y concisa la información clave para que cualquier persona pueda iniciarse en OSM y por otro lado servir de herramienta de aprendizaje tanto para los lectores como los redactores. Y es que es una excelente forma de aprender documentando: el proceso es el mismo que el que seguiríamos para aprender algo que a priori nos interesa (buscar información, preguntar dudas en foros o grupos de correo específicos, consultarnos a nosotros... ), con la salvedad de que en lugar de anotarlo en nuestros apuntes, lo anotaremos en un manual colaborativo que otros podrán consultar y aprender de ello.

Además, es una forma de dejar de ser sujetos pasivos para convertirnos en sujetos activos y ofrecer un retorno a la comunidad OSM (la idea es publicar el libro bajo licencia abierta y donarlo a la comunidad).

## ¿Dónde puedo encontrar el manual?

Tanto el manual (todavía en desarrollo) como las instrucciones de participación las puedes encontrar en este enlace: [https://mapcolabora.gitbooks.io/manual-osm/content/](https://mapcolabora.gitbooks.io/manual-osm/content/)

## ATENCIÓN, proyecto sin continuidad

Esta iniciativa del grupo ya no tiene continuidad y alguno de los contenidos pueden estar desactualizados. Para más información sobre OpenStreetMap te aconsejamos que visites [wiki.osm.org](https://wiki.osm.org) o te pongas en contacto con la comunidad española en la que participamos activamente en las [diferentes vías](https://wiki.openstreetmap.org/wiki/ES:Espa%C3%B1a#Contacto) que tenemos.
