+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 15  # Order that this section will appear.

title = "¿Qué es Mapeado Colaborativo?"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"

  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"

  # Background image.
  # image = ""  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false
[advanced]
 # Custom CSS.
 css_style = "padding-top: 20px; padding-bottom: 20px;"

 # CSS class.
 css_class = ""
+++

Es un grupo de entusiastas que se reúnen para hablar y trabajar en torno a la
construcción y elaboración de información geográfica participativa o voluntaria, para
construir mapas útiles a la ciudadanía: que se puedan usar como herramientas para una
mejor toma de decisiones, para compartir experiencias y ayudar a configurar el modo
con el que observamos la realidad.

Mapeado Colaborativo nació en marzo de 2016 dentro de los [grupos residentes en Zaragoza Activa - Las Armas](http://blogzac.es/category/think-zac-las-armas/grupos-residentes/) como _grupo de investigación/acción para la innovación ciudadana y la canalización de la inteligencia colectiva_. Posteriormente, debido a su similitud y conexión con otros grupos que surgieron de la comunidad de usuarios de [Software Libre Geoespacial](https://osgeo.org), se sumó a la iniciativa de [Geoinquietos](https://geoinquietos.org).

El propósito del grupo es generar mapas colaborativos para beneficiar a la ciudadanía. Este fin se logra través de la concienciación de la relevancia la información geográfica en la vida cotidiana y promoviendo herramientas y conocimientos que capaciten a colectivos y personas para llevar a cabo procesos de generación de de información geográfica de manera voluntaria y participativa.
