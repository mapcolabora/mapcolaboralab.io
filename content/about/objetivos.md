+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Objetivos del grupo"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"

  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"

  # Background image.
  # image = ""  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS.
 css_style = "padding-top: 20px; padding-bottom: 20px;"

 # CSS class.
 css_class = ""
+++

* Proporcionar **herramientas y conocimientos para empoderar a distintos colectivos y personas** en el proceso de generación información geográfica voluntaria y participativa.
* Consolidar un **grupo de trabajo** con personas interesadas por el territorio (tanto en el ámbito urbano como en el rural), la información geográfica, la cartografía y las iniciativas colaborativas. En suma, consolidar un grupo para el mapeado colaborativo **usando herramientas y datos libres**.
* Establecer una **red de colaboraciones** entre distintos colectivos y personas afines al grupo de trabajo.
* **Tomar conciencia** de la importancia y el poder de la información geográfica en casi todos los aspectos de nuestras vidas.
