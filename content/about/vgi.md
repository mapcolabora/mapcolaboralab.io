+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "¿Qué entendemos por mapeado colaborativo?"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"

  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"

  # Background image.
  image = "headers/grupo-zac.jpg"  # Name of image in `static/img/`.
  image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

[advanced]
 # Custom CSS.
 css_style = "padding-top: 20px; padding-bottom: 20px;"

 # CSS class.
 css_class = "wg-blank-white-text"
+++

En realidad el término original viene de la **información geográfica voluntaria** (*volunteer geographical information, VGI,* en inglés) que consiste en la incorporación de información espacial proporcionada de manera participativa por personas de todo tipo gracias a la aplicación de tecnologías como la localización por satélite (GNSS) disponible en los teléfono móviles, el desarrollo de numerosas herramientas de software libre (FOSS) y código abierto y la proliferación de la web 2.0 con la proliferación de plataformas en Internet para la difusión de información como la Wikipedia.

Uno de los máximos exponentes de la difusión y crecimiento de la información geográfica voluntaria es el proyecto [OpenStreetMap](https://openstreetmap.org) que es es una base de datos espacial colaborativa que viene a hacer las funciones de la Wikipedia pero en términos de elementos espaciales y que constituye la más grande y con mayor plataforma cartográfica global cuya licencia es libre (ODbL).
