+++
title = "My GEOCHICAS: Mapeando las calles de las mujeres en Zaragoza."
subtitle = ""

date = 2020-02-12T09:25:02
lastmod = 2020-02-14T09:25:02
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla"]

tags = ["feminismo"]
summary = ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "Zaragoza ya está en el proyecto 'Las Calles de las Mujeres'"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

  # Show image only in page previews?
  preview_only = false

+++

Taller, Charla en la Universidad de Zaragoza (Campus San Fracisco). Facultad de Educación. Aulas 0.01 y 0.02, Zaragoza. 13 de febrero.

El próximo 13 de febrero, el proyecto [My GEO “Geo Tools for Modernization and Youth Employment”](https://www.mygeoproject.eu/) junto con Mapeado Colaborativo y el Colegio de Geógrafos, quiere llevar a cabo en Zaragoza una interesante propuesta emprendida por **[GeoChicasOSM](https://geochicas.org/)** (situando en el ya existente [Mapa de las Calles de las Mujeres](https://geochicasosm.github.io/lascallesdelasmujeres/) de diferentes ciudades de habla hispana (Latinoamérica y España) a Zaragoza.

El objetivo es por un lado visibilizar la brecha que existe históricamente en la representación de figuras femeninas en las calles de las ciudades y por otro, normalizar el uso de los Sistemas de Información Geográfica para la difusión y puesta en valor de la mujer.

{{< tweet 1228018763880189952 >}}

{{< tweet 1228022213103751173 >}}


{{% alert note %}}
A 14 de febrero de 2020 podemos decir que Zaragoza ya está en "Las Calles delas Mujeres": https://geochicasosm.github.io/lascallesdelasmujeres/#zaragoza
{{% /alert %}}
