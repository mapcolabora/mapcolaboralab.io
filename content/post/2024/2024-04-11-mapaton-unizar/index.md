+++
title = "Mapatón Humanitario"
subtitle = "Mapatón humanitario para ayudar a Médicos Sin Fronteras en la Universidad de Zaragoza" 
authors = ["miguel-sevilla"]
date = 2024-04-22T10:00:00
tags = ["actividades", "Universidad de Zaragoza", "Mapatón Humanitario"]
# summary = ""
draft = false

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "Participantes del mapatón"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

  # Show image only in page previews?
  preview_only = false
+++

El pasado jueves 11 de abril realizamos junto con el [Departamento de Geografía y Ordenación del Territorio de la Universidad de Zaragoza](https://geografia.unizar.es) y [Médicos Sin Fronteras España](https://msf.es) un _Mapatón Humanitario_ para apoyar a la ONG en un área donde están trabajando para llevar ayuda médica básica.

Como en otras ocasiones (con esta es la quinta vez que lo hacemos<!-- INCLUIR enlaces? -->) nos reunimos en dos salas con ordenadores de la universidad y comenzamos nuestra tarea que no es otra más que ayudar a mejorar la cartografía de una zona determinada en OpenStreetMap.

Desde Médicos Sin Fronteras, en concreto Juan José (Paco) Arévalo, el responsable de la cartografía y sistemas de información geográfica de esta ONG en España, nos proveyó de una tarea a realizar en el [estado de Mizoram en el extremo oriental de la India](https://es.wikipedia.org/wiki/Mizoram), en el contacto con [Myanmar](https://es.wikipedia.org/wiki/Birmania) donde hay una gran cantidad de desplazado por los conflictos de este último país.

A continuación incluimos el vídeo de presentación de Paco Arévalo:

{{< youtube 178vpuXt2BI >}}

.

Tras la presentación de Paco, comentamos a los participantes la manera de proceder. Para ello nos valimos de una presentación que se puede seguir en [este enlace](./20240411-mapaton-humanitario.pdf) y que define los pasos a seguir y que son los siguientes:

- darse de [alta en OpenStreetMap](https://www.openstreetmap.org/user/new) para tener un usuario registrado para realizar cambios en esta plataforma,

- entrar en la [tarea 16509](https://tasks.hotosm.org/projects/16509/) dela plataforma del [equipo humanitario de OpenStreetMap, HOT](https://www.hotosm.org/) donde había incluido Médicos Sin Fronteras una de sus prioridades.

- introducir su usuario de OpenStreetMap para iniciar la actividad

- elegir, dentro de la tarea, un polígono a editar

- y una vez dentro del editor web, [iD](https://wiki.openstreetmap.org/wiki/ES:ID), que se abre automáticamente, identificar, sobre la imagen de alta resolución que nos presenta, edificaciones e incluirlas. Para ello iD nos presenta unas simples herramientas que permiten delimitar estas edificaciones y calificarlas como tal, o sea, asignarles la etiqueta y valor `building=yes` propia de [edificaciones genéricas](https://wiki.openstreetmap.org/wiki/ES:Edificios).

- una vez identificadas todas las edificaciones del polígono asignado y tras ir guardando los cambios, hay que indicar que ese polígono está completo o en su caso comentar si ha habido alguna incidencia.

- y se puede proseguir en otros polígonos de la tarea siguiendo las indicaciones de más arriba.

Después de finalizar las cuatro horas y dos sesiones del mapatón los resultados obtenidos eran muy interesantes. Habíamos superado con creces más de dos terceras partes de la tarea, habían participado cerca de 90 usuarios diferentes y se habían añadido a OpenStreetMap más de 10.500 edificios que serán claves para localizar las áreas habitadas en esta zona remota para coordinar las tareas de ayuda humanitaria de Médicos Sin Fronteras en el terreno.

Las cifras de la tarea pueden verse en la propia página del gestor de HOT en el [apartado de estadísticas](https://tasks.hotosm.org/projects/16509/stats) y que recogemos en imágenes a continuación:

![Estadísticas generales](./task_16509_stats_1.png)

![Contribuidores](task_16509_stats_2.png)

![Tiempo y ejecución](task_16509_stats_3.png)

Para terminar añadimos aquí enlaces a:

- [Página de registro del evento de la Universidad](https://eventos.unizar.es/114491/detail/mapaton-humanitario-zaragoza-2024.html) con la información previa que aportamos a los participantes.

- [Noticia en la web del departamento de Geografía](http://geografia.unizar.es/noticia/el-mapaton-humanitario-confirma-el-interes-por-la-funcion-solidaria-de-la-geografia) que recoge algunas apariciones en prensa del mapatón.
