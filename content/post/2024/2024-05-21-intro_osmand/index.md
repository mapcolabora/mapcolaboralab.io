+++
title = "Introducción a OSMAnd"
subtitle = "OSMAnd, alternativa libre a Google Maps con muchas funciones" 
authors = ["miguel-sevilla"]
date = 2024-05-21T10:00:00
tags = ["OSMAnd", "OpenStreetMap", "Google Maps"]
# summary = ""
draft = false

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "Captura de OSMAnd de mi móvil"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

  # Show image only in page previews?
  preview_only = false
+++

**ATENCIÓN esta entrada es actualmente un borrador pendiente de ser finalizada.**

[OSMAnd](https://osmand.net/) es una aplicación imprescindible para aquellas personas entusiastas de las herramientas y los datos libres espaciales como los que nos reunimos en [Mapeado Colaborativo/Geoinquietos Zaragoza](https://mapcolabora.org). Esta aplicación como su nombre indica, está destinada a usar los datos de nuestra base de datos espaciales libre preferida, la cartografía de [OpenStreetMap](https://osm.org), "OSM", en en el sistema operativo Android, "And", aunque ahora también es posible instalarla en iOS. Esto es, viene a ser una alternativa libre a la [aplicación privativa](https://es.wikipedia.org/wiki/Software_propietario) y los [datos cerrados de Google Maps](https://wiki.openstreetmap.org/wiki/Google), pues el código de la aplicación está bajo licencia [GPLv3](https://es.wikipedia.org/wiki/GNU_General_Public_License) y los datos que usa principalmente son los [datos libres de OSM bajo licencia ODbL](https://osmfoundation.org/wiki/Licence).

En este post vamos a realizar una pequeña introducción de cómo manejar y sacar partido a algunas de las características de OSMAnd.

## Instalación

Si vais a la [página principal de OSMAnd](https://osmand.net/) o buscáis la aplicación en el gestor de aplicaciones de vuestro móvil encontraréis que hay una versión sencilla, con restricciones, que es gratuita y otra llamada **OSMAnd+**, de pago, que no tiene restricciones.

Aunque es recomendable comprar la versión de pago o "plus", que tiene un precio mínimo por las ventajas y el uso que le vamos a dar, existe una alternativa, realmente libre y que es la versión que hay en el repositorio de herramientas libres para Android, (no para iOS ☹ ) **[F-Droid](https://f-droid.org/)**, que se llama **[OSMAnd~](https://f-droid.org/packages/net.osmand.plus/)** y que es una versión de OSMAnd sin restricciones. Si tienes instalado F-Droid (muy recomendable si tienes Android) simplemente búscalo allí. Si no puedes descargar el archivo `apk`, el archivo para instalar la aplicación, en la página de F-Droid de [OSMAnd~](https://f-droid.org/packages/net.osmand.plus/).

![Buscando OSMAnd en F-Droid](./fdroid.jpg)

Adicionalmente, si nos interesa tener las curvas de nivel para ver la cartografía con ellas, tendremos que instalarnos OsmAnd Contour lines, disponible igualmente en [F-Droid](https://f-droid.org/en/packages/net.osmand.srtmPlugin.paid/) o tu gestor de aplicaciones del móvil.

## Descarga de mapas online

Una vez que empezamos a usar OSMAnd veremos una interfaz muy similar a la de otras aplicaciones de mapas en el móvil que te animo a que descubras poco a poco porque creo que la parte más básica es común a todas estas aplicaciones y si quieres profundizar en detalle [tienes la guía de usuario de la aplicación en su web](https://osmand.net/docs/user/).

![Interfaz de OSMAnd](./osmand_main.jpg)

Para empezar, nos localizará y nos sugerirá descargar los mapas del área en el que nos encontramos pues a diferencia de otras aplicaciones y creo que es lo más ventajoso de esta, es que podemos descargarnos la cartografía en formato vectorial (que ocupa poco) en nuestro dispositivo y usarlo **sin conexión**.

![Sugerencia de descarga del mapa](./osmand_download_map.jpg)

<!-- así sale texto debajo:
{{< figure src="./osmand_download_map.jpg" title="Sugerencia de descarga del mapa" >}}
-->

Si no nos apareciera esta opción siempre podemos ir nosotros a **descargar el contenido que nos interese para tenerlo sin conexión**, no solo la cartografía de OpenStreetMap, si no también las curvas de nivel o los puntos de artículos de la Wikipedia como se muestra en la imagen.

![Descargas de OSMAnd](./osmand_downloads.jpg)

Para llegar a este apartado hay que acceder al menú principal de la aplicación a través de las tres rallas,'≡', que aparecen en el área inferior izquierda de la pantalla y una vez allí en "Mapas y recursos" o similar (según tengas tu idioma, el mío es en inglés como puedes ver en las imágenes).

![Menú principal](./osmand_main_menu.jpg)

A partir de aquí ya puedes empezar a interaccionar con el mapa. Recuerda que toda la documentación, aunque en inglés, está disponible en el [manual oficial de la aplicación en la red](https://osmand.net/docs/user/map/interact-with-map).

## Añadir mapas extras

Una de las cuestiones más relevantes de OSMAnd para mi es que no solo puedo usar la base cartográfica de OpenStreetMap en formato vectorial y sin conexión si no que te posibilita incluir otras capas como la ortofotografía aérea o la serie de mapas topográficos del IGN dentro de la aplicación, entre otras.

![Configuración del mapa](./osmand_configure_map.jpg)

Para llegar hasta esta opción, dentro del menú principal hay que ir a la configuración del mapa y allí en "fuente de mapa", tendremos que activar el plugin "mapas online, y luego podremos proseguir nuevamente y nos dará varias opciones y al final tendremos podremos elegir (1) "añadir más" o (2) "añadir manualmente". En la primera opción podremos añadir mapas predefinidos generales muy interesantes, sin embargo lo que quiero mostrar es que podemos añadir cualquier servicio de teselas de internet, [TMS](https://en.wikipedia.org/wiki/Tile_Map_Service), a nuestro OSMAnd.

![Añadir recurso online](./osmand_add_online_source.jpg)

Añadir esta clase de servicios a nuestra aplicación es tan sencillo como saber la URL del mismo y cambiar las letras `x`, `y` y `z` por los números `1`, `2` y `0` respectivamente.

Incluyo más abajo el texto con las direcciones para añadir diferentes servicios de teselas a OSMAnd que me resultan de interés:

```default
Imágenes del Plan Nacional de Ortofotografía Aérea del IGN
IGN PNOA
http://www.ign.es/wmts/pnoa-ma?request=GetTile&service=WMTS&VERSION=1.0.0&Layer=OI.OrthoimageCoverage&Style=default&Format=image/png&TileMatrixSet=GoogleMapsCompatible&TileMatrix={0}&TileRow={2}&TileCol={1}

Mápas en formato ráster del IGN
IGN Mapa topográfico
http://www.ign.es/wmts/mapa-raster?request=GetTile&service=WMTS&VERSION=1.0.0&Layer=MTN&Style=default&Format=image/png&TileMatrixSet=GoogleMapsCompatible&TileMatrix={0}&TileRow={2}&TileCol={1}

ESRI Satellite
(tiene las imágenes del PNOA con algo menos de resolución pero es más rápido)
https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{0}/{2}/{1}

Google Maps
(variante para mapas web)
https://mt1.google.com/vt/lyrs=r&x={1}&y={2}&z={0}

Google Traffic
(capa con el tráfico a color de las carreteras y con transparencia)
https://mt1.google.com/vt?lyrs=h@159000000,traffic|seconds_into_week:-1&style=3&x={1}&y={2}&z={0}

```

Esta cartografía por servicios TMS dependen de la conexión de tu dispositivos pero al tener OSMAnd una memoria caché, una cantidad de datos que almacena para usar con posterioridad sin tener que conectarse, **si consultamos el área de interés poco antes de quedarnos sin conexión, estas teselas estarás disponibles en nuestra aplicación cuando estemos en el área de estudio** si no hay cobertura.

Tienes más información sobre los mapas online en el [manual de OSMAnd](https://docs.osmand.net/docs/user/map/raster-maps/) y puedes buscar más servicios TMS en diferentes páginas web del tema como [esta](https://mappinggis.com/2018/03/como-anadir-mapas-base-en-qgis-3-0-openstreetmap-google-carto-stamen).

## Navegación y búsquedas

Como no podía ser menos OSMAnd nos permite trazar rutas para navegar con él. Puedes ver [más información en el manual](https://osmand.net/docs/user/navigation/) pero quiero hacer hincapié aquí en que esta creación de rutas, *routing* en inglés, las realiza sobre la base cartográfica vectorial que te has descargado lo que, de nuevo, te libera de tener conexión a internet en tu dispositivo, y te permite realizarlas sobre una cartografía que, para el área rural es muy completa pues te incluye pistas, caminos y senderos. Así mismo no hay que olvidar que la cartografía es la de OpenStreetMap que puedes mejorarla [uniéndote a la comunidad de editores de este proyecto](https://wiki.openstreetmap.org/wiki/ES:C%C3%B3mo_contribuir).

Las rutas las puedes ir realizando añadiendo los puntos a mano sobre el mapa o buscando, usando la lupa que sale en la pantalla principal, las direcciones o los elementos geográficos que estén en la base de datos de OSM.

![Buscando fuentes](./osmand_search_drinking.jpg)

De este modo podremos localizar:
- Direcciones como `Calle de La Peña de Oroel, 12, Zaragoza`
- O, por el apartado de categorías, las fuentes en una ciudad (ver ejemplo)
- O directamente incluir las coordenadas de un punto en diferentes formatos: `41.663755° N, -0.868129° E` ó ` N 41° 39' 49.518 E -0° 52' 5.2644` incluso en [coordenadas UTM](https://osmand.net/docs/user/search/search-coordinates/)

Con estas búsquedas puedes iniciar la navegación en coche, en bicicleta, caminando, en transporte público o incluso a caballo (si la información está recogida adecuadamente en OpenStreetMap).

- - -

PENDIENTE DE ACTUALIZAR

## Crear favoritos, exportar e importar

- Presionar sobre la localización dada
- Salvar como favorito
- Gestionar los marcadores favoritos
- Exportar como GPX (formato de intercambio de archivos GPS)
- Importar desde GPX

## Extras

![Representación 3D en OSMAnd](./osmand_3d.jpg)

OSMAnd es una herramienta muy completa y que nos va a permitir muchas más cosas como editar o incluir puntos en la propia base de datos de OpenStreetMap, ver el mapa en relieve y en tres dimensiones o personalizar las visualización de los elementos del mapa con diferentes estilos.
