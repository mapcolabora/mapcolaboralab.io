+++
title = "Nuevas actividades del grupo en Zaragoza Activa 2024"
subtitle = "Volvemos a tener actividades presenciales" 
authors = ["miguel-sevilla"]
date = 2024-04-22T00:00:00
tags = ["actividades", "Zaragoza Activa"]
# summary = ""
draft = false

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "Edificio de La Azucarera"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

  # Show image only in page previews?
  preview_only = false
+++

Como consecuencia de la pandemia y la desaparición de los grupos residentes en Zaragoza Activa - Las Armas desde el 2020 no teníamos actividades presenciales pero este año 2024 y de la mano, de nuevo, de Zaragoza Activa, que nos vió nacer, pero ahora en [La Azucarera](https://www.openstreetmap.org/way/336142920) nos hemos liado la manta a la cabeza y hemos empezados a organizar de nuevo talleres y reuniones.

El pasado año aprovechando que se iba a realizar la conferencia sobre cultura libre, esLibre, en Zaragoza, nos liamos la manta a la cabeza y coordinamos una [sala de charlas sobre datos y herramientas libres espaciales](https://mapcolabora.org/post/2023/2023-05-12-eslibre/). Aquella experiencia y el éxito que tuvo nuestra sala fue el detonante para realizar algunas reuniones informales y, más tarde, dar el paso de buscar un espacio para retomar las reuniones del grupo de manera formal y con una regularidad establecida. Y es que teniendo esto a buen seguro podremos retomar proyectos ya iniciados y activar nuevas ideas.

Hemos tenido la suerte que la propuesta de actividades que hicimos a Zaragoza Activa fue aceptada y desde el martes 2 de abril hemos empezado a reunirnos en [La Azucarera](https://www.openstreetmap.org/way/336142920), intentando tener una periodicidad de al menos una vez al mes, e intentando quedar cada dos semanas, los martes de 18:30 a 20:30.

Nuestro planteamiento sigue siendo el mismo que el que nos llevó a crear el grupo, investigar, colaborar y ayudar a las personas interesadas sobre la temática de los datos y las herramientas libres espaciales.

Aunque en el día que se está escribiendo esta entrada ya hemos realizado tres actividades, voy a dejar aquí las ideas que planteamos en el mensaje que se envió a la [lista de correos del grupo](https://lists.osgeo.org/mailman/listinfo/mapcolabora). Pego aquí algunas de las ideaas, que podrían variar y son para empezar a pensar, que comentamos a Zaragoza Activa en nuestra propuesta de actividades:

- Introducción y edición colaborativa de los datos de OpenStreetMap en directa conexión con el resto de la comunidad española de editores de esta iniciativa en la que nos integramos o en coordinación con otros grupos de Geoinquietos.

- Mapeado de la red peatonal urbana en los alrededores de La Azucarera para evaluar la accesibilidad del área para peatones ordinarios y personas con diversidad funcional en la línea del proyecto #Zaccesible;

- Realización de charlas de autoformación para el uso y la introducción de herramientas libres espaciales como QGIS o la creación de mapas web con uMap o Leaflet

- Aplicación y entrenamiento de herramientas de deep learning e inteligencia artificial para la identificación de elementos espaciales usando imágenes alta resolución y su incorporación en los datos de OpenStreetMap

- Proyecto de #Ziclabilidad de la mano del proyecto STARS con colaboraciones con el IES La Azucarera e IES Itaca

- Realización de _mapatones_, reuniones para cartografiar de manera intensiva áreas de interés en OpenStreetMap. Pueden ser de  carácter humanitario, como el que [ya realizamos junto con departamento de Geografía y Ordenación del Territorio de la Universidad de Zaragoza](https://eventos.unizar.es/go/MapatonMSFGeografia2024) para ayudar a los proyectos de Médicos Sin Fronteras.

- Participación en otras actividades y eventos de temáticas afines de Zaragoza Activa.

Esperamos que estas ideas sean de interés y podamos disfrutar, como ya hemos podido comprobar hasta el momento de la presencia de un grupo interesado por nuestras "inquietudes libres espaciales".

Y por terminar comentar que en este curso seguiremos publicitando nuestras actividades en las redes sociales, ya no solo en nuestra cuenta de [X/Twitter](https://twitter.com/mapcolabora) si no también en nuestra nueva [cuenta de Mastodon](https://mapstodon.space/@mapcolabora/) y la recientemente abierta de [Instagram](https://www.instagram.com/mapeadocolaborativo/).
