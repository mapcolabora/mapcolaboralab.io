+++
title = "LibreGeo: Geoinquietos, OpenStreetMap, QGIS y otras aplicaciones libres espaciales"
subtitle = "Mapeado colaborativo está en esLibre"
date = 2023-05-12T09:25:02
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["miguel-sevilla", "carlos-camara"]

tags = ["actividades", "cultura libre", "OpenStreetMap"]
# summary = ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "Cartel y logotipo de esLibre"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

  # Show image only in page previews?
  preview_only = false

+++

Como usuarios y convencidos del software libre no podíamos faltar en la edición de esLibre 2023. Además, dado que este año tiene lugar en nuestra ciudad, Zaragoza, hemos querido organizar una sala dedicada a aplicaciones espaciales libres, que hemos titulado: **"LibreGeo: Geoinquietos, OpenStreetMap, QGIS y otras aplicaciones libres espaciales"**.

El objetivo de esta sala es la de dar cabida a la cultura libre en torno a los datos espaciales: tanto (1) en la creación de datos espaciales libres, como es el de la comunidad de colaboradores de OpenStreetMap, como (2) los usuarios y desarrolladores de aplicaciones libres de geomáticas, como pueden ser QGIS u otras herramientas libres para el tratamiento de datos espaciales. Puedes encontrar la información sobre la sala en la página de su propuesta.

Puedes ver la sala en este enlace: <https://propuestas-libregeo.eslib.re/2023/> , así como más información, aquí también: <https://propuestas.eslib.re/2023/salas/libregeo-geoinquietos-openstreetmap-qgis-otras>

A continuación listamos el contenido que tenemos preparado:

## Charlas

- Jose Lozano - [Cálculo de balance hídrico a través del modelado de datos espaciales, en la cuenca del río Motatán, Estado Trujillo](https://propuestas-libregeo.eslib.re/2023/charlas/calculo-balance-hidrico-modelado-datos-espaciales)
- Carmen Díez SanMartín - [Las calles de la mujeres en Aragón por Geochicas OSM](https://propuestas-libregeo.eslib.re/2023/charlas/calles-mujeres-aragon-geochicasosm)
- Iván Sánchez Ortega - [Gleo, o cómo reinventar mapas con WebGL](https://propuestas-libregeo.eslib.re/2023/charlas/gleo-como-reinventar-mapas-webgl)
- Víctor Martínez - [Herramientas libres de un SIG con Información Geografica Voluntaria](https://propuestas-libregeo.eslib.re/2023/charlas/herramientas-libres-sig-informacion-geografica-voluntaria)
- Maria Caudevilla Lambán - [Metodología libre para la elaboración de cartografía turística](https://propuestas-libregeo.eslib.re/2023/charlas/metodologia-libre-elaboracion-cartografia-turistica)
- Fergus Reig Gracia - [NcWebMapper](https://propuestas-libregeo.eslib.re/2023/charlas/ncwebmapper)
- Joan Cano Aladid - [El uso de OpenDroneMap en proyectos de fotogrametría aérea con drones](https://propuestas-libregeo.eslib.re/2023/charlas/opendronemap-proyectos-fotogrametria-aerea-drones)
- yopaseopor - [OSMPoisMap y Tagcionario: acercando las etiquetas al público general](https://propuestas-libregeo.eslib.re/2023/charlas/osmpoismap-tagcionario-acercando-etiquetas-publico-general)
- Rafael Martínez Cebolla - [Repositorio de código abierto de ICEARAGON](https://propuestas-libregeo.eslib.re/2023/charlas/repositorio-codigo-abierto-icearagon)

## Talleres

- Antonio Montes (inventadero) - [Edición del Paisaje en realidad extendida (XR)](https://propuestas-libregeo.eslib.re/2023/talleres/edicion-paisaje-realidad-extendida)
- Carlos López Quintanilla - [Introducción a QGIS](https://propuestas-libregeo.eslib.re/2023/talleres/introduccion-qgis)
- Miguel Sevilla-Callejo - [Taller de introducción a la edición en OpenStreetMap](https://propuestas-libregeo.eslib.re/2023/talleres/taller-introduccion-edicion-openstreetmap)
