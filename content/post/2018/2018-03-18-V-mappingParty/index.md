+++
title = "Próxima V mapping party #Zaccesibilidad"
subtitle = "Inicio de la IV Mapping Party en el barrio del Arrabal"

date = 2018-03-18T10:00:00
lastmod = 2018-01-13T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["carlos-camara"]

tags = ["zaccesible", "mapping party", "anuncio", "OpenStreetMap"]
summary = ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""

  # Show image only in page previews?
  preview_only = false

+++

El próximo viernes 23 de marzo organizamos la V Mapping Party recopilando datos de accesibilidad para el proyecto [Zaragoza Accesible](/project/zaccesible), esta vez en el barrio de Las Fuentes.

Recordamos que las [mapping parties](/tags/mapping-party/) son jornadas abiertas a todo el mundo en el que voluntarios se reúnen para mapear de forma colaborativa y coordinada a través de la plataforma [OpenStreetMap](http://openstreetmap.org).

El objetivo de esta actividad es identificar las barreras arquitectónicas de una zona en concreto para posteriormente subirlas a la plataforma anteriormente citada y que estén en disposición de todo el que necesite dicha información.

En esta actividad los participantes ayudarán a hacer de su ciudad más accesible, ya que con esta toma de datos serán partícipes del proyecto [Zaragoza Accesible](/project/zaccesible) mediante el cual se pretende tener todas las barreras arquitectónicas de Zaragoza reflejadas en un Mapa a disposición de todo aquel que lo necesite.

Antes de iniciar la sesión realizaremos una breve explicación de la metodología a seguir, crearemos grupos de mapeo y empezaremos con la toma de datos y para finalizar la jornada haremos una puesta en común comentando impresiones.

## ¿Quién puede venir?

Todo el mundo está invitado y es bienvenido a unirse, cualquier persona que esté interesada en el tema de la accesibilidad en el espacio público y tenga ganas de participar pasará una agradable jornada.

Además, si no tiene conocimientos previos de mapeo ¡es una buena ocasión para adquirirlos!.

Creemos que es una buena oportunidad para tener una primera toma de contacto práctica con los mapeos colaborativos, a la vez que se contribuye con el trabajo personal a mejorar la integración de personas con discapacidad en el ámbito urbano.

## Datos del evento

* FECHA: Viernes 23 de marzo
* HORA: 09.30h
* LUGAR: [Centro Cívico Salvador Allende](https://www.openstreetmap.org/relation/8088925) (C. Florentino Ballesteros, 8)

<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-0.8718600869178773%2C41.643993063307484%2C-0.8673807978630067%2C41.645784956664365&amp;layer=mapnik&amp;marker=41.644889016215124%2C-0.8696204423904419" style="border: 0px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=41.64489&amp;mlon=-0.86962#map=19/41.64489/-0.86962">View Larger Map</a></small>
