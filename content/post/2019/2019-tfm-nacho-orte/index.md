+++
title = "TFM Nacho Orte"
subtitle = "Te invitamos a las sesiones de este curso"

date = 2019-11-25T09:25:02
lastmod = ""
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["admin", "Nacho Orte", "miguel-sevilla","carlos-camara"]

tags = ["accesibilidad", "mapeado colaborativo", "investigación"]
summary = ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "Una sesión de Mapeado Colaborativo en ZAC las Armas."

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

  # Show image only in page previews?
  preview_only = false

+++

En el 2019 nuestro compañero **Nacho Orte** ha defendido su TFM titulado como _“Análisis de la movilidad peatonal urbana para Personas con Diversidad Funcional a través del uso de Información Geográfica Voluntaria"_, y dirigido por [Miguel Sevilla Callejo](/authors/miguel-sevilla) y [Carlos Cámara Menoyo](/authors/carlos-camara).
En su trabajo, analiza las desigualdades de los espacios urbanos que se recrean con respecto a la accesibilidad en la red de movilidad peatonal urbana y su interacción con las personas con diversidad funcional, tanto sillas de ruedas como invidentes.

Resultados del análisis espacial de la accesibilidad en parte de los barrios de Las Fuentes y San José con los datos recogidos por el grupo y presentados en el Trabajo de Fin de Máster de Ignacio Orte a finales de 2019.

Los distritos municipales objetos de estudio fueron el de Las Fuentes y San José, los cuales presentan un contexto demográfico envejecido y un marco urbanístico susceptible de ser inaccesible.

Los análisis fueron realizados con datos de OpenStreetMap, utilizados para diagnosticar la la vialidad de las aceras y los pasos de peatones para la accesibilidad.
Gracias a la actividad del grupo Mapeado Colaborativo, con el desarrollo de cada mapping party se ha podido depurar y mejorar la metodología de toma de datos y de trabajo. En este sentido, la mapping party en Las Fuentes ha marcado un punto de inflexión, y ha dado lugar a un adelanto y puesta en valor, no solo social, si no académico: el Trabajo de Fin de Grado, primero, y luego el TFM de Ignacio Orte Sierra, en el que se ha empezado a experimentar con el cálculo de rutas accesibles mediante SIG.
Parte de este trabajo se ha visto traducido en comunicaciones en congresos como el Congreso de Vida Independiente y artículos científicos  (algunos pendientes de publicación).
