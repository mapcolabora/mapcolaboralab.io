+++
title = "Inicio del 5º curso en ZAC"
subtitle = "Te invitamos a las sesiones de este curso"

date = 2019-05-06T09:25:02
lastmod = 2019-05-07T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["carlos-camara"]

tags = ["zaragoza Activa", "mapeado colaborativo"]
summary = ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["mapaton-humanitario"]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = "Una sesión de Mapeado Colaborativo en ZAC las Armas."

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"

  # Show image only in page previews?
  preview_only = false

+++

Aunque hace ya algunas semanas que empezó nuestro 5º curso en ZAC las Armas (y lo hicimos a lo grande, coordinando un nuevo Mapatón Humanitario en el que nos acompañaron otras 15 ciudades españolas) queríamos contaros algunas de las cosas que queremos hacer durante este curso e invitaros a uniros a las actividades e incluso proponernos algunas nuevas. Pero antes, os recordamos quiénes somos y qué hacemos.

## ¿Qué es el grupo de Mapeado Colaborativo?

Es un grupo de entusiastas que se reúnen para hablar y trabajar en torno a la construcción y elaboración de información geográfica participativa o voluntaria, para construir mapas útiles a la ciudadanía: que se puedan usar como herramientas para una mejor toma de decisiones, para compartir experiencias y ayudar a configurar el modo con el que observamos la realidad.

## ¿Por qué es necesario Mapeado Colaborativo?

Reunirnos para hablar y trabajar alrededor de la información geográfica participativa o voluntaria va más allá de compartir el tiempo y el conocimiento con gente con intereses afines. A modo de ejemplo, cuando creamos el grupo nos planteamos los siguientes objetivos:

* Proporcionar herramientas y conocimientos para empoderar a distintos colectivos y personas en el proceso de generación información geográfica voluntaria y participativa
* Consolidar un grupo de trabajo con personas interesadas por el territorio (tanto en el ámbito urbano como en el rural), la información geográfica, la cartografía y las iniciativas colaborativas.
* Establecer una red de colaboración entre distintos colectivos y personas afines al grupo de trabajo.
* Tomar conciencia de la importancia y el poder de la información geográfica en casi todos los aspectos de nuestras vidas


## ¿Qué hemos hecho hasta ahora y que pretendemos seguir haciendo?

El grupo lleva activo como grupo de investigación/acción en Zaragoza Activa - Las Armas desde marzo de 2016 y desde entonces se ha estado trabajando en diversas temáticas como:

* la accesibilidad peatonal urbana, [#zaccesibilidad](/project/zaccesible/);
* la infraestructura ciclista urbana, [#ziclabilidad](/project/ziclabilidad/);
* realizando actividades puntuales como charlas y talleres sobre temática diversa relacionada con la información geográfica voluntaria o maratones de mapeado con fines humanitarios; [#mapatonhumanitario](/project/mapaton-humanitario/).

{{< figure src="photo_2019-05-06_10-55-33.jpg" title="Sesión de Zaragoza del Mapatón humanitario 2019, con Héctor supervisando una de las 2 aulas, totalmente llena." >}}

Este curso seguiremos planteando sesiones sobre estos proyectos, además de hacer sesiones formativas para la capacitación y empoderamiento (como la de introducción a OpenStreetMap que tenemos planteada para el próximo jueves día 9 de mayo). Además, como siempre, estamos abiertos a que nos propongáis vuestras inquietudes y necesidades, para lo cual os invitamos a que vengáis a vernos o nos escribáis a la lista de correo.

## Os recordamos nuestro horario y dónde encontrarnos:

Horario: de 17:30 a 20:30

Lugar: ZAC Las Armas. Calle las Armas, 72. Zaragoza.

Próximas sesiones:

* Jueves 9 de mayo
* Jueves 23 de mayo
* Jueves 13 de junio
* Jueves 27 de junio


## Enlaces y recursos del grupo

WEB: http://mapcolabora.org

MEETUP de actividades: https://www.meetup.com/mapcolabora/

Cuenta de twitter: @MapColabora

Repositorio de archivos en Github: https://github.com/mapcolabora

Email: info@mapcolabora.org
