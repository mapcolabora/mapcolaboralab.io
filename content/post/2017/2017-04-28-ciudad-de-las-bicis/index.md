+++
title = "Presentación del proyecto Ziclabilidad en la ciudad de las bicis"
subtitle = ""

date = 2017-04-28T10:00:00
lastmod = 2018-01-13T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["hector-ochoa"]

tags = ["presentaciones", "ziclabilidad", "OpenStreetMap"]
summary = ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["ziclabilidad"]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""

  # Show image only in page previews?
  preview_only = false

+++

El pasado 28 de abril, dentro del marco de [La ciudad de las bicis](http://laciudaddelasbicis.org), Alejandro Suárez presentó la comunicación "El uso de una plataforma cartográfica libre, OpenStreet Map, para el mapeado colaborativo de la ciclabilidad de Zaragoza", que formó parte de las comunicaciones seleccionadas al XIV Congreso Ibérico dentro del eje Innovación y Desarrollo Económico.

A continuación dejamos la presentación:

<iframe src="https://docs.google.com/presentation/d/1n3-xEJXI50uaiAG83A18JeAmN3EM9qkzGLEDOcFVYiE/embed?start=false&loop=false&delayms=3000" frameborder="0" width="750" height="590" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
