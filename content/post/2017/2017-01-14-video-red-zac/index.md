+++
title = "Vídeo para la Red ZAC"
subtitle = "En este vídeo de 2 minutos te contamos lo que hacemos."

date = 2017-01-14T10:47:00
lastmod = 2018-01-13T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["carlos-camara"]

tags = ["zaragoza activa"]
summary = "La gente de La Truca Estudio hizo unos vídeos de 2 minutos sobre los grupos de Zaragoza Activa y estamos muy contentos de cómo quedó el nuestro."

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""

  # Show image only in page previews?
  preview_only = false

+++

El pasado mes de enero, desde [Zaragoza Activa](http://blogzac.es/) encargaron la realización de una serie de vídeos para explicar qué hacemos cada uno de sus [grupos residentes](https://www.zaragoza.es/ciudad/sectores/activa/grupos-residentes.htm). Los vídeos, realizados por [La Truca Estudio](http://www.latrucaestudio.es/), tienen una duración de dos minutos y muestran de forma muy resumida y clara las actividades que realizamos. Y como estamos muy contentos y agradecidos con el resultado, compartimos el vídeo que han hecho sobre Mapeado Colaborativo

{{< youtube id="iVk5Dyx4Cgk" autoplay="false" >}}

Y si queréis saber más sobre el proyecto ThinkZac, aquí tenéis los vídeos del resto de grupos:

* [Hackeo Urbano de Espacios](https://www.youtube.com/watch?v=X9KrhKgSAwc)
* [Software Cívico](https://www.youtube.com/watch?v=jZcXD85cWfU)
* [Tráfico de ideas](https://www.youtube.com/watch?v=rfY97Uyujuc)
* [Economías Feministas](https://www.youtube.com/watch?v=Tn69HwiGd-0)
* [Ecología Urbana](https://www.youtube.com/watch?v=UZx3eh2m2dY)
