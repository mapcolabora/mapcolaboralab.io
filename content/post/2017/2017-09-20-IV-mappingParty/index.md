+++
title = "Crónica de la IV mapping party #Zaccesibilidad"
subtitle = "Inicio de la IV Mapping Party en el barrio del Arrabal"

date = 2017-09-20T10:00:00
lastmod = 2018-01-13T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["joan-cano"]

tags = ["zaccesible", "mapping party", "crónica", "OpenStreetMap"]
summary = ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""

  # Show image only in page previews?
  preview_only = false

+++

El pasado sábado 16 de septiembre iniciamos el nuevo curso participando en la **IV Mapping Party #Zaccesibilidad** en el barrio del Arrabal, la cual fue impulsada por el proyecto “[Ganchillo Social](https://elganchillosocial.wordpress.com/)”.
Además, nos acompañó con su colaboración [Ocio Inclusivo](https://ocioinclusivoarrabal.wordpress.com/) y la Asociación de Vecinos del Arrabal (muchas gracias!).

A las 11h daba comienzo la sesión, con una gran explicación de [Noe](https://twitter.com/ganchillosocial) sobre los grupos y ciudadanos que nos habíamos reunido y el objetivo común qué nos movía: la detección de barreras arquitectónicas que existen en las ciudades relativas a la accesibilidad y su análisis mediante cartografía colaborativa.
{{< tweet 908988329210892288 >}}

Como es de costumbre, se repartieron los [fieldpapers](http://fieldpapers.org/atlases/3p5u7pu2) entre 4 grupos a ritmo de la dulzaina aragonesa para empezar a trabajar sobre ellos, anotando cada elemento con sus particulares carcaterísticas en la hojas de trabajo que preparamos.

{{< tweet 909018555882049536 >}}

Gracias a la colaboración de los participantes, se pudieron poner en común diferentes visiones de los elementos arquitectónicos relacionados con la accesibilidad: pavimento en los cruces de peatones, semáforos, anchuras de las aceras, etc.

Entre parada y parada la sesión se nos pasó volando!, con conversaciones enriquecedoras y apreciaciones a simple vista invisibles que nos hacen adquirir experiencia y aprender de ellas.

Es impresionante como mediante la diversidad en grupos de trabajo se consigue obtener información geográfica de valiosísimo valor, la cual, posteriormente puede transformarse en cartografía y sobretodo en acciones que reviertan en dichos grupos.

Por otra parte, la visibilización de las tecnologías de la información geográfica a pie de calle, fomenta la aportación de ideas y proyectos que fácilmente podrían llegar a una simbiosis. Es un punto que tal vez se debería plantear un futuras sesiones, pues pueden salir muy buenos proyectos.

Por último, dar las gracias a todos los grupos y ciudadanos asistentes que con su granito de aportación social, ayudan a romper barreras cada día, llenando de gratitud las mentes de aquellos que cada día se topan con una.

Ah! Recordaros que el próximo viernes nos reuniremos para subir los datos de la Mapping Party en el horario habitual: de 10 a 13 en la [sede de Zaragoza Activa en las Armas](http://www.openstreetmap.org/node/2353638893). ¡Estáis todos invitados!

[Enlace a Red ZAC](https://www.zaragoza.es/zac/events/41942;jsessionid=5651e421351058aaa74f222d2f38b56ccd3484da96d156d2297688218346c0b6.e34LaxmMchmNe34Lch0PchiSax90)
