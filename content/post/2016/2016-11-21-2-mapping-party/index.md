+++
title = "2ª Mapping Party #Zaccesibilidad"
subtitle = "Celebramos el día internacional de la persona con discapacidad con una mapping party."

date = 2016-11-16T00:00:00
lastmod = 2018-01-13T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["cesar-canalis"]

tags = ["zaccesible", "OpenStreetMap"]
summary = "Celebramos el día internacional de la persona con discapacidad con una mapping party."

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["zaccesible"]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder.
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""

  # Show image only in page previews?
  preview_only = false

+++

El día 3 de Diciembre, coincidiendo con  el Día Mundial de las Personas con Discapacidad, tendrá lugar una *Mapping Party* realizada por el grupo de trabajo de Mapeado Colaborativo residente en Zaragoza Activa junto a la ONG Discapacitados Sin Fronteras y a la cual os invitamos a participar.

## ¿Qué es el Grupo de Mapeado Colaborativo?

Un grupo residente en Zaragoza Activa que nace en Marzo de 2016 como grupo de investigación/acción para la innovación ciudadana y la canalización de la inteligencia colectiva. Su fin es  la creación de mapas colaborativos al servicio de la ciudadanía, a través de la concienciación sobre la importancia de la información geográfica en nuestro día a día y la promoción de herramientas y conocimientos para que colectivos y personas tengan la capacidad de realizar procesos de generación de información geográfica voluntaria y participativa.

## ¿Cuál es el fin del evento?

Una Mapping Party es un evento abierto a todo el mundo en el que voluntarios se reúnen para mapear de forma colaborativa y coordinada a través de la plataforma [OpenStreetMap](http://openstreetmap.org).

El objetivo de esta actividad es identificar las barreras arquitectónicas de una zona en concreto para posteriormente subirlas a la plataforma anteriormente citada y que estén en disposición de todo el que necesite dicha información.

En esta actividad los participantes ayudarán a hacer de su ciudad más accesible, ya que con esta toma de datos serán partícipes del proyecto [Zaragoza Accesible](project/zaragoza-accesible) mediante el cual se pretende tener todas las barreras arquitectónicas de Zaragoza reflejadas en un Mapa a disposición de todo aquel que lo necesite.

Antes de iniciar la sesión realizaremos una breve explicación de la metodología a seguir, crearemos grupos de mapeo y empezaremos con la toma de datos y para finalizar la jornada haremos una puesta en común comentando impresiones.

## ¿Quién puede venir?

Todo el mundo está invitado y es bienvenido a unirse, cualquier persona que esté interesada en el tema de la accesibilidad en el espacio público y tenga ganas de participar pasará una agradable jornada.

Además, si no tiene conocimientos previos de mapeo ¡es una buena ocasión para adquirirlos!.

Creemos que es una buena oportunidad para tener una primera toma de contacto práctica con los mapeos colaborativos, a la vez que se contribuye con el trabajo personal a mejorar la integración de personas con discapacidad en el ámbito urbano.

## Datos del evento

* FECHA: Sábado 3 de Diciembre
* HORA: 10.00h
* LUGAR: Plaza de la Magdalena
